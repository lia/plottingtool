import os
import ROOT
import atlasplots as aplt
#from helper import ofufbins, create_default_legend, calculate_separation_index

path = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL-UMICH-VBFs.root'
outDir = '/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/plots_FitInputs_V19_PCBT'
histos = {}
Yields = {}
histosVBFSR = {}
YieldsVBFSR = {}
BDTs = ["SMVBFBDTScore_V02","SMVBFBDTScore_V03","SMVBFBDTScore_V07"]
Channels = ["hhttbbVBFSM"]
Regions = ["VBFSR"]
colors = [ROOT.kRed+1, ROOT.kBlue+1, ROOT.kMagenta+1, ROOT.kOrange+1, ROOT.kYellow+1, ROOT.kCyan+1, ROOT.kGreen+1]

if not os.path.exists(outDir): os.makedirs(outDir)
#preselection
df = ROOT.RDataFrame("Nominal", path)
df = df.Filter('mBB < 150000.')
df = df.Filter("n_btag==2")
df = df.Filter('OS==1')
df = df.Filter('!sample.empty()')
df = df.Filter('mHH>0.')
#df = df.Filter("hasVBFJetCandidates==1")
#Print basic informations
print(f"-->Working on the input file {path}")
print(f"-->Will work on the following samples: {Channels}")
print(f"-->Will work on the following BDT scores: {BDTs}")
print(f"-->Will work on the following Regions: {Regions}")

#setup style
ROOT.gROOT.SetBatch()
aplt.set_atlas_style()

for cur_region in Regions:
  print(f"---->Working on the region {cur_region}")
  if cur_region == 'VBFSR':
    df_region = df.Filter('region==1')
  elif cur_region == 'GGFSR':
    df_region = df.Filter('region==0')
  else:
    print(f"---->Region {region} is not supported for the moment, please choose 'VBFSR' or 'GGFSR'")
  for cur_sample in Channels:
    print(f"---->Working on the sample {cur_sample}")
    df_sample = df_region.Filter(f'(sample == "/{cur_sample}"/')

        
        
  exit(0)      
  #initial the plotting
  fig, ax = aplt.subplots(1, 1)
  for cur_K2V, cur_color in zip(K2Vs, colors):
    print(f"------>Working on the K2V: {cur_K2V}")
    df_tmp = df
    df_tmp = df_tmp.Filter('(sample == \"{}\")'.format(cur_K2V))
    h = (df_tmp.Histo1D(f"{cur_var}", "weight"))
    #h = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{cur_var}_{cur_K2V}", f"{cur_var}", 200, -1, 1), f"{cur_var}", "weight"))
    Yields[cur_K2V] = df_tmp.Sum('weight').GetValue()
    histos[cur_K2V] = h.GetValue().Clone()
    histos[cur_K2V].Scale(1./Yields[cur_K2V])
    ax.plot(histos[cur_K2V], label=cur_K2V, linecolor = cur_color,  labelfmt="L")
  
  ax.add_margins(top=0.30)
  # Set axis titles
  ax.set_xlabel(cur_var)
  ax.set_ylabel("Normalised to unity")
  # Add the ATLAS Label
  aplt.atlas_label(text="Internal", loc="upper left")
  ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
  ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-}", size=22, align=13)
  # Add legend
  #ax.legend(loc=(0.50, 0.50, 0.95, 0.92))
  ax.legend(loc=(0.45, 0.55, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))
  fig.savefig(os.path.join(outDir,f"K2V_Compare_{cur_var}.pdf"))
  


