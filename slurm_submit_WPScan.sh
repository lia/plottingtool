#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --mem=4G
#SBATCH --time=0-01:00:00
#SBATCH --array=0-100
#SBATCH --output=/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/BATCH/job_%A_%a.out
#SBATCH --error=/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/BATCH/job_%A_%a.err

param=$(printf "%.2f" "$(echo "scale=2; -1 + 0.01 * $SLURM_ARRAY_TASK_ID" | bc)")

bash /sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/job.sh -- $param
