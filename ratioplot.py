import ROOT

# Set the ATLAS style
ROOT.gROOT.SetStyle("ATLAS")
ROOT.gROOT.ForceStyle()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPaperSize(20, 26)
ROOT.gStyle.SetPadTopMargin(0.05)
ROOT.gStyle.SetPadRightMargin(0.05)
ROOT.gStyle.SetPadBottomMargin(0.16)
ROOT.gStyle.SetPadLeftMargin(0.16)
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetHistMinimumZero()

# ATLAS label
ATLAS_label = '#font[72]{ATLAS} #font[42]{Internal}'
ROOT.gStyle.SetPaintTextFormat("4.2f")
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptStat(0)


# Set the font sizes
ROOT.gStyle.SetTextFont(42)
ROOT.gStyle.SetTextSize(0.05)
ROOT.gStyle.SetLabelFont(42, "XYZ")
ROOT.gStyle.SetLabelSize(0.05, "XYZ")
ROOT.gStyle.SetTitleFont(42, "XYZ")
ROOT.gStyle.SetTitleSize(0.05, "XYZ")
ROOT.gStyle.SetTitleOffset(1.2, "X")
ROOT.gStyle.SetTitleOffset(1.5, "Y")
ROOT.gStyle.SetTitleOffset(1.2, "Z")

# ATLAS label position
ROOT.gStyle.SetLabelOffset(0.025, "Y")
ROOT.gStyle.SetLabelSize(0.045, "Y")
ROOT.gStyle.SetTitleOffset(1.5, "Y")
ROOT.gStyle.SetTitleSize(0.05, "Y")
ROOT.gStyle.SetTitleSize(0.06, "X")
ROOT.gStyle.SetTitleFont(62, "X")
ROOT.gStyle.SetLabelSize(0.05, "X")
ROOT.gStyle.SetLabelFont(42, "X")

# create canvas
canvas = ROOT.TCanvas("canvas", "Canvas", 800, 600)

# open root files
file1 = ROOT.TFile.Open("/atlas/data19/guhitj/lxplus/bbtautau/k2vcheck/WSMaker_HH_bbtautau/inputs/PCBT_BDT2_Test1/13TeV_TauLH_2tag_0mHH_OS_VBFSR_SMVBFBDTScore.root")
file2 = ROOT.TFile.Open("/atlas/data19/guhitj/lxplus/bbtautau/k2vcheck/WSMaker_HH_bbtautau/inputs/PCBT_BDT2_Test1_1000/13TeV_TauLH_2tag_0mHH_OS_VBFSR_SMVBFBDTScore.root")

# get histograms
hist1 = file1.Get("hhttbb")
hist2 = file2.Get("hhttbb")

# normalize histograms
hist1.Scale(1/hist1.Integral())
hist2.Scale(1/hist2.Integral())

# rebin histograms
#hist1.Rebin(5)
#hist2.Rebin(5)

# set line colors and styles
hist1.SetLineColor(ROOT.kBlue)
hist1.SetLineStyle(1)
hist2.SetLineColor(ROOT.kRed)
hist2.SetLineStyle(2)

# create top pad
top_pad = ROOT.TPad("top_pad", "Top Pad", 0, 0.3, 1, 1)
top_pad.SetTopMargin(0.1)
top_pad.SetBottomMargin(0.01)
top_pad.SetLeftMargin(0.12)
top_pad.SetRightMargin(0.04)
top_pad.Draw()

# create bottom pad
bottom_pad = ROOT.TPad("bottom_pad", "Bottom Pad", 0, 0, 1, 0.3)
bottom_pad.SetTopMargin(0.01)
bottom_pad.SetBottomMargin(0.3)
bottom_pad.SetLeftMargin(0.12)
bottom_pad.SetRightMargin(0.04)
bottom_pad.Draw()

# draw histograms on top pad
top_pad.cd()
hist1.Draw("hist")
hist2.Draw("hist same")

# add legend
legend = ROOT.TLegend(0.75, 0.7, 0.95, 0.9) #x1, y1, x2, y2
legend.AddEntry(hist1, "hhttbb vbf prev", "l")
legend.AddEntry(hist2, "hhttbb vbf 1000", "l")
legend.Draw()

#Add ATLAS Label
tex = ROOT.TLatex(0.19, 0.85, ATLAS_label)
tex.SetNDC()
#tex.SetTextFont(75)
tex.SetTextSize(0.05)
tex.Draw()

sqrt_s_label = ROOT.TLatex(0.19, 0.78, "#sqrt{s} = 13 TeV, 139 fb^{-1}")
sqrt_s_label.SetNDC()
sqrt_s_label.SetTextSize(0.05)
sqrt_s_label.Draw()

hh_label = ROOT.TLatex(0.19, 0.71, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-}")
hh_label.SetNDC()
hh_label.SetTextSize(0.05)
hh_label.Draw()

# set axis labels and title
hist1.GetXaxis().SetTitle(" ")
hist1.GetYaxis().SetTitle("a.u (normalised)")
#hist1.SetTitle("Title")

# remove stats box
ROOT.gStyle.SetOptStat(0)

# draw ratio on bottom pad
bottom_pad.cd()
ratio = hist1.Clone()
ratio.Divide(hist2)
ratio.SetLineColor(ROOT.kBlack)
ratio.Draw("hist")
ratio.SetTitle("")
ratio.GetYaxis().SetTitle("Ratio")
ratio.GetYaxis().SetRangeUser(0., 3.5)
ratio.GetYaxis().SetNdivisions(505)
ratio.GetYaxis().SetTitleSize(0.12)
ratio.GetYaxis().SetLabelSize(0.1)
ratio.GetXaxis().SetTitleSize(0.15)
ratio.GetXaxis().SetLabelSize(0.12)
ratio.GetXaxis().SetTitleOffset(1.1)
ratio.GetYaxis().SetTitleOffset(0.5)

# add horizontal line at ratio = 1
horizontal_line = ROOT.TLine(ratio.GetXaxis().GetXmin(), 1, ratio.GetXaxis().GetXmax(), 1)
horizontal_line.SetLineColor(ROOT.kRed)
horizontal_line.SetLineStyle(2)
horizontal_line.Draw()

# save canvas as pdf
canvas.SaveAs("output_hhttbb_vbf_1000.pdf")

# close root files
file1.Close()
file2.Close()


'''
import ROOT
ROOT.gStyle.SetOptStat(0)
# Open the two root files and get the histograms
file1 = ROOT.TFile("/atlas/data19/guhitj/lxplus/bbtautau/k2vcheck/WSMaker_HH_bbtautau/inputs/EB_BDT2_ggfvbf_Round2/13TeV_TauLH_2tag_350mHH_OS_GGFSR_SMGGFBDTScore.root")
file2 = ROOT.TFile("/atlas/data19/guhitj/lxplus/bbtautau/k2vcheck/WSMaker_HH_bbtautau/inputs/PCBT_BDT2_Test1/13TeV_TauLH_2tag_350mHH_OS_GGFSR_SMGGFBDTScore.root")
histo1 = file1.Get("hhttbbL10")
histo2 = file2.Get("hhttbbL10")

# Set the style for the histograms
histo1.SetLineColor(ROOT.kRed)
histo1.SetLineWidth(2)
histo2.SetLineColor(ROOT.kBlue)
histo2.SetLineWidth(2)

# Rebin the histograms
histo1.Rebin(8)
histo2.Rebin(8)

# Normalize the histograms to unity
histo1.Scale(1.0/histo1.Integral())
histo2.Scale(1.0/histo2.Integral())

# Create the canvas and pads
canvas = ROOT.TCanvas("canvas", "canvas", 800, 800)
pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1)
pad1.SetBottomMargin(0)
pad1.Draw()
pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, 0.3)
pad2.SetTopMargin(0)
pad2.SetBottomMargin(0.2)
pad2.Draw()

# Draw the histograms in the top pad
pad1.cd()
histo1.Draw("HIST")
histo2.Draw("SAME HIST")

# Set the legend
legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
legend.AddEntry(histo1, "hhttbb FixedCut", "l")
legend.AddEntry(histo2, "hhttbb PCBT", "l")
legend.Draw()

# Draw the ratio in the bottom pad
pad2.cd()
ratio = histo1.Clone()
ratio.Divide(histo2)
ratio.SetTitle("")
ratio.SetMarkerStyle(20)
ratio.SetMarkerSize(1)
ratio.GetYaxis().SetTitle("Ratio")
ratio.GetYaxis().SetTitleSize(0.15)
ratio.GetYaxis().SetTitleOffset(0.5)
ratio.GetYaxis().SetLabelSize(0.12)
ratio.GetYaxis().SetNdivisions(505)
ratio.GetXaxis().SetTitleSize(0.15)
ratio.GetXaxis().SetLabelSize(0.12)
ratio.GetXaxis().SetTitleOffset(1)
ratio.GetXaxis().SetNdivisions(510)
ratio.SetMinimum(0.5)
ratio.SetMaximum(1.5)
ratio.Draw("EP")

# Draw a horizontal line at ratio=1
line = ROOT.TLine(ratio.GetXaxis().GetXmin(), 1, ratio.GetXaxis().GetXmax(), 1)
line.SetLineColor(ROOT.kBlack)
line.SetLineStyle(2)
line.Draw()

# Save the canvas
canvas.SaveAs("output_hhttbb_lowmhh.pdf")
'''
