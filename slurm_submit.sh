#!/bin/sh
#SBATCH --ntasks=8
#SBATCH --mem=32G
#SBATCH --time=10:00:00
#SBATCH --output=/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/BATCH/slurm.out
bash /sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/job.sh
