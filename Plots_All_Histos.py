import numpy as np
import atlasplots as aplt
import ROOT
import os
from datetime import datetime

# Set up style
ROOT.gROOT.SetBatch()
aplt.set_atlas_style()

# Define the input file and histogram names for each process
input_file = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/Reader_EBR_V02/hist-ALL-extsh2211.root"
processes = {
    "ggF": ['hhttbb'],
    "ggFKL10": ['hhttbbL10'],
    "VBFSM": ['hhttbbVBFSM'],
    "Diboson": ["WZ", "ZZ", "WW"],
    "Zttjets": ["Zttbb", "Zttbl", "Zttbc", "Zttl", "Zttcc", "Zttcl"],
    "Zjets": ["Zcl", "Zcc","Zbl", "Zbb", "Zbc"],
    "Stop": ["stops", "stopt", "stopWt"],
    "ttV": ["ttW", "ttZ"],
    #"DY": ["DY", "DYtt"],
    "SingleHiggs": ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau"],
    "ttbar": ["ttbar"],
    "Others": ["Zcl", "Zcc","Zbl", "Zbb", "Zbc", "Zl", "W", "W", "Wtt","DY", "DYtt"],
    "Fakes": ["WFake", "WttFake", "ttbarFake"],

}

# Define the signal regions
signal_regions = {
    "low-mHH-GGFSR": ["2tag_0_350mHH_OS_GGFSR"],
    "high-mHH-GGFSR": ["2tag_350mHH_OS_GGFSR"],
    "VBFSR": ["2tag_0_350mHH_OS_VBFSR", "2tag_350mHH_OS_VBFSR"]
}
# Define the variables
vars = ["mHH","SMVBFBDTScore_V03_PCBT_extsh2211_2000", "SMGGFBDTScore_test2_extsh2211_2000", "SMGGFBDTScore_mHHsplit_test2_extsh2211_2000"]
#vars = ["mHH"]
# Define the luminosity (in fb-1)
luminosity = 139

# Define the colors for each process using ROOT colors
processes_colors = {
    "ggF": ROOT.TColor.GetColor("#E6194B"),
    "ggFKL10": ROOT.TColor.GetColor("#3CB44B"),
    "VBFSM": ROOT.TColor.GetColor("#FFE119"),
    "Diboson": ROOT.TColor.GetColor("#4363D8"),
    "Zttjets": ROOT.TColor.GetColor("#F58231"),
    "Zjets": ROOT.TColor.GetColor("#911EB4"),
    "Stop": ROOT.TColor.GetColor("#46F0F0"),
    "ttV": ROOT.TColor.GetColor("#F032E6"),
    "DY": ROOT.TColor.GetColor("#BCF60C"),
    "SHiggs": ROOT.TColor.GetColor("#FABEBE"),
    "ttbar": ROOT.TColor.GetColor("#008080"),
    "Others": ROOT.TColor.GetColor("#E6BEFF"),
    "Fakes": ROOT.TColor.GetColor("#9A6324"),
}


# Open the ROOT file using ROOT.TFile
root_file = ROOT.TFile.Open(input_file)
print(f"--> Reading ROOT file: {input_file}")

# Access the MVAInputs and FitInputs TDirectories
mva_inputs = root_file.Get("MVAInputs")
fit_inputs = root_file.Get("FitInputs")
print(type(mva_inputs))
print(type(fit_inputs))
# Create the output directory
output_dir = f"./plots_EBR_V02_{datetime.now().strftime('%Y%m%d')}"
os.makedirs(output_dir, exist_ok=True)
rebin_factor = 50
# Loop through the variables, signal regions, and processes
for var in vars:
    print(f"--> Working on variable: {var}")
    for sr, sr_names in signal_regions.items():
        print(f"---> Working on signal region: {sr}")
        # Initialize the plotting
        fig, ax = aplt.subplots(1, 1)

        for process, hist_names in processes.items():
            print(f"----> Working on process: {process}")
            # Initialize a variable to store the combined histogram values
            combined_hist = None

            for hist_name in hist_names:
                # Construct the full histogram name
                full_hist_name_list = [f"{hist_name}_{sr_name}_{var}" for sr_name in sr_names]
                #print(full_hist_name_list)
                for full_hist_name in full_hist_name_list:
                    print(f"-----> Accessing histogram: {full_hist_name}")

                    # Check if the histogram is in mva_inputs or fit_inputs and read it
                    if "BDT" in full_hist_name:
                        hist = fit_inputs.Get(full_hist_name)
                        if bool(hist):
                            hist.Rebin(rebin_factor)
                    else:
                        hist = mva_inputs.Get(full_hist_name)
                    if not bool(hist):
                        print(f"-----> Error: Histogram {full_hist_name} not found in MVAInputs or FitInputs")
                        continue
                    if combined_hist is None:
                        # If this is the first histogram, create the combined histogram with the same properties
                        combined_hist = hist.Clone()
                    else:
                        # Otherwise, add the histogram to the existing combined histogram
                        combined_hist.Add(hist)

            if combined_hist is None:
                print(f"----> Error: No histogram combined for process: {process}")
                continue

            # Normalize the combined histogram
            combined_hist.Scale(1./combined_hist.Integral())
            # Plot the ROOT.TH1F object using atlasplots
            ax.plot(combined_hist, label=process, linecolor=processes_colors[process], label_fmt='L')
        # Add a legend and set the labels
        ax.add_margins(top=0.30)
        # Set axis titles
        ax.set_xlabel(f"{var}")
        ax.set_ylabel("Normalised to unity")
        # Add the ATLAS Label
        aplt.atlas_label(text="Internal", loc="upper left")
        ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
        ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau_{lep}#tau_{had}", size=22, align=13)
        ax.text(0.2, 0.74, sr, size=22, align=13)
        
        # Add legend
        #ax.legend(loc=(0.50, 0.50, 0.95, 0.92))
        ax.legend(loc=(0.55, 0.35, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))

        # Save the plot without showing it
        output_filename = os.path.join(output_dir,f"{sr}_{var}_overlayed_histograms.pdf")
        print(f"-----> Saving plot: {output_filename}")
        fig.savefig(output_filename)

