# PlottingTool

## Getting started

```
git clone ssh://git@gitlab.cern.ch:7999/lia/plottingtool.git
cd plottingtool
source setup_worker.sh
sh install_venv.sh
source setup_local.sh
```

## Each time login
```
source setup_worker.sh
source setup_local.sh
```

## To compare any distribution of different K2V value 
```
python K2V_Comparison.py
```

## To quickly produce a single ggF and VBF SR using the tree (output of CxAODReader_bbtautau) 
```
python Quick_ggFVBFCat_production_V03.py /sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V16.root hist-mc16ade-SM-SLT-ALL_APC_V16_WPm0p2.root -0.2
```
## To quickly produce a scan of ggF and VBF SR using the tree (output of CxAODReader_bbtautau) 
```
python Quick_ggFVBFCat_production_list_V03.py /sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V16.root hist-mc16ade-SM-SLT-ALL_APC_V16_list.root
```
Recommand to submit jobs to batch system to parellel the jos

## To scan the significance on ggF/VBF BDT WP:
```
python Significance.py
```

## Other tools for HHbbtautau analysis

- [ ] [CxAODReader_bbtautau](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/cxaodreader_bbtautau)
- [ ] [HbbHtautauMLToolkit] (https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/hbbhtautaumltoolkit)
- [ ] [WSMaker_HH_bbtautau] (https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker_HH_bbtautau)


## Project status
Still need to add:
1, a tool to check the cutflow

# Workflow for Boosted Decision Tree (BDT) Working Point (WP) Scan in Gluon-Gluon Fusion (ggF)/Vector Boson Fusion (VBF)

## 1. Preparing ntuples from CxAODReader_HH_bbtautau

Firstly, save all the relevant samples into a single root file, `tree_ALL.root`. After that, isolate the ggF related samples and save them in another root file named `tree-hhbbtt.root`.

## 2. Production of Histograms (Inputs for WSMaker) with Various WPs

Use the following command in your terminal to start producing histograms:

```python
python Quick_ggFVBFCat_production_V03.py
```

Please note, while this command will scan over the working points, it is not recommended due to its inefficiency. The scan occurs sequentially, which can be time-consuming.

To improve efficiency, consider submitting jobs to your batch system. You can vary the working point as needed and submit jobs in parallel. For instance, to run the scan for WP=-0.13, use the following command:

```python
python Quick_ggFVBFCat_production_V03.py --WP -0.13 
```

The outputs of this command will be formatted appropriately for input into the WSMaker.

## 3. Feeding Histograms into WSMaker

Once your histograms are ready, you can feed them into the WSMaker to calculate the various limits.

## 4. Generating Final Summary Plots

To generate summary plots, use the `Scan_Kappa_plots.py` script as shown below:

```python
python Scan_Kappa_plots.py
```

This will provide you with a visual representation of the data analyzed throughout this workflow.
