import os
import ROOT
import atlasplots as aplt
import uproot
from datetime import datetime
from pathlib import Path
from multiprocessing import Pool

def process_var(cur_var):
    InPut =  "/sps/atlas/a/angli/ATLAS/bbtautau/storage/EB_Reader_dev/tree-ALL.root"
    outDir = f"../outdir_plottingtool/plots_tree_ALL_{datetime.now().strftime('%Y%m%d')}"
    processes = {
        "ggF": ['hhttbb'],
        "ggFKL10": ['hhttbbL10'],
        "VBFSM": ['hhttbbVBFSM'],
        "Diboson": ["WZ", "ZZ", "WW"],
        "Zttjets": ["Zttbb", "Zttbl", "Zttbc", "Zttl", "Zttcc", "Zttcl"],
        "Zjets": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc"],
        "Stop": ["stops", "stopt", "stopWt"],
        "ttV": ["ttW", "ttZ"],
        ##"DY": ["DY", "DYtt"],
        "SHiggs": ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau"],
        "ttbar": ["ttbar"],
        "Others": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc", "Zl", "W", "W", "Wtt","DY", "DYtt"],
        "Fakes": ["WFake", "WttFake", "ttbarFake"],
    }
    # Create a dictionary for process colors
    process_colors = {
        "ggF": ROOT.kRed+1,
        "ggFKL10": ROOT.kBlue+1,
        "VBFSM": ROOT.kMagenta+1,
        "Diboson": ROOT.kOrange+1,
        "Zttjets": ROOT.kGreen+1,
        "Zjets": ROOT.kYellow+1,
        "Stop": ROOT.kCyan+1,
        "ttV": ROOT.kMagenta-7,
        "DY": ROOT.kRed-6,
        "SHiggs": ROOT.kBlue-7,
        "ttbar": ROOT.kGreen+4,
        "Others": ROOT.kGreen+1,
        "Fakes": ROOT.kGray,
    }

    BDT = "SMBDT_ggF_VBF"
    WP = -0.13
    PreSelection="OS==1&&n_btag==2&&mBB<150000"
    signal_regions = {
        "low-mHH-GGFSR":    f"mHH<350000.&&Category(hasVBFJetCandidates,{BDT},{str(WP)})==0",
        "high-mHH-GGFSR":   f"mHH>350000.&&Category(hasVBFJetCandidates,{BDT},{str(WP)})==0",
        "VBFSR": f"Category(hasVBFJetCandidates,{BDT},{str(WP)})==1",
    }

    variable_binnings = {
        #"TauEta": (50, -5, 5),
        #"LepEta": (50, -5, 5),
        #"mHH": (80, 0, 800000),
        "SMBDT_ggF_inclusive": (200, -1, 1),
        "SMBDT_ggF": (200, -1, 1),
        "SMBDT_ggF_ADA": (200, -1, 1),
        "SMBDT_VBF": (200, -1, 1),
        "SMBDT_ggF_VBF": (200, -1, 1),
        #"SMVBFBDTScore_V03_PCBT_extsh2211": (50, -1, 1),
        #"SMGGFBDTScore_test2_extsh2211": (50, -1, 1),
        #"SMGGFBDTScore_mHHsplit_test2_extsh2211": (50, -1, 1),
        #"mjjVBF": (60, 0, 600000),
        #"dEtajjVBF": (50, 0, 5),
        #"cent_ttjf": (50, 0, 1),
        #"dRjjVBF": (50, 0, 10),
        #"multEtajjVBF": (50, 0, 10),
        #"mEff_ttjf": (100, 0, 5000000),
        #"fwm0_ttj": (50, 0.95, 1),
        #"dEtaHH": (50, 0, 6),
        #"Mb0lep": (50, 0, 300000),
        #"hcm7_bbtt": (50, 0, 1),
        #"fwm4_bbtt": (50, 0, 1),
        #"dPtLepTau": (50, 0, 500000),
        #"hcm3_ttjf": (50, 0, 1),
        #"SubLeadBjet": (80, 0, 300000),
        #"TauPt": (50, 0, 500000),
        #"dPhiLepMET": (50, -3.14, 3.14),
        #"pTSixObjVBF": (50, 0, 100000),
        #"mBB": (50, 0, 150000),
        #"mMMC": (50, 0, 300000),
        #"dRBB": (50, 0, 5),
        #"dRTauLep": (50, 0, 5),
        #"fwm2_ttj": (50, 0, 1),
        #"Mb1lep": (50, 0, 300000),
        #"mHH_scaled": (50, 0, 800000),
        #"LeadBjet": (50, 0, 300000),
    }

    #variable_binnings = {
    #    "mHH": (80, 0, 800000),
    #    "SMVBFBDTScore_V03_PCBT_extsh2211": (50, -1, 1),
    #    "SMGGFBDTScore_test2_extsh2211": (50, -1, 1),
    #    "SMGGFBDTScore_mHHsplit_test2_extsh2211": (50, -1, 1),
    #    "mjjVBF": (60, 0, 600000),
    #    "dEtajjVBF": (50, 0, 5),
    #    "cent_ttjf": (50, 0, 1),
    #    "dRjjVBF": (50, 0, 10),
    #    "multEtajjVBF": (50, 0, 10),
    #    "mEff_ttjf": (100, 0, 5000000),
    #    "fwm0_ttj": (50, 0.95, 1),
    #    "dEtaHH": (50, 0, 6),
    #    "Mb0lep": (50, 0, 300000),
    #    "hcm7_bbtt": (50, 0, 1),
    #    "fwm4_bbtt": (50, 0, 1),
    #    "dPtLepTau": (50, 0, 500000),
    #    "hcm3_ttjf": (50, 0, 1),
    #    "SubLeadBjet": (80, 0, 300000),
    #    "TauPt": (50, 0, 500000),
    #    "dPhiLepMET": (50, -3.14, 3.14),
    #    "pTSixObjVBF": (50, 0, 100000),
    #    "mBB": (50, 0, 150000),
    #    "mMMC": (50, 0, 300000),
    #    "dRBB": (50, 0, 5),
    #    "dRTauLep": (50, 0, 5),
    #    "fwm2_ttj": (50, 0, 1),
    #    "Mb1lep": (50, 0, 300000),
    #    "mHH_scaled": (50, 0, 800000),
    #    "LeadBjet": (50, 0, 300000),
    #    "n_jets": (50, 0, 10),
    #    "HT": (50, 0, 3000000),
    #    "T1": (50, -25, 25),
    #    "T2": (50, -25, 25),
    #    "ST": (50, 0, 5000000),
    #    "MET": (50, 0, 1000000),
    #    "MT2": (50, 0, 500000),
    #    "pTBB": (50, 0, 500000),
    #    "pTHH": (50, 0, 500000),
    #    "mTtau": (50, 0, 500000),
    #    "mTLep": (50, 0, 500000),
    #    "LepPt": (50, 0, 500000),
    #    "cosTheta": (50, -1, 1),
    #    "pTTauLep": (50, 0, 500000),
    #    "SubLeadBjet": (50, 0, 300000),
    #    "dPhibbMET": (50, -3.14, 3.14),
    #    "dPhiLepTauBB": (50, -3.14, 3.14),
    #    "dPhiTauTauMET": (50, -3.14, 3.14),
    #    "mHHStar": (50, 0, 800000),
    #    "dPhiLepMET": (50, -3.14, 3.14),
    #    "dPhiTauMET": (50, -3.14, 3.14),
    #    "cent_bbtt": (50, 0, 1),
    #    "mEff_ttj": (50, 0, 5000000),
    #    "fwm1_bbtt": (50, 0, 1),
    #    "spher_bbtt": (50, 0, 1),
    #    "pflow_bbtt": (50, 0, 1),
    #    "mTW": (50, 0, 500000),
    #    "coshelicitybb": (50, -1, 1),
    #    "coshelicitytautau": (50, -1, 1),
    #    "mHH_scaled": (50, 0, 800000),
    #    "TauEta": (50, -2.5, 2.5),
    #    "LepEta": (50, -2.5, 2.5),
    #    "dRb0tau": (50, 0, 5),
    #    "dRb1tau": (50, 0, 5),
    #    "dRb0lep": (50, 0, 5),
    #    "dRb1lep": (50, 0, 5),
    #    "quant_b0": (5, 0.5, 5.5),
    #    "quant_b1": (5, 0.5, 5.5),
    #}


    

    ROOT.gInterpreter.Declare("""
      int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
        int m_region = -1;
        if(!hasVBFJetCandidates){
          m_region = 0;
        }else{
          if(SMBDTggFVBF > cut ){
            m_region = 0;
          }else{
            m_region = 1;
          }
        }
        return m_region;
      }
      """)


    Path(outDir).mkdir(parents=True, exist_ok=True)


    #preselection
    df = ROOT.RDataFrame("Nominal", InPut)
    df = df.Filter(PreSelection)
    print(f"-->Working on the input file {InPut}")
    print(f"-->Will work on the following processes: {processes}")
    print(f"-->Will work on the following variables: {vars}")

    #setup style
    ROOT.gROOT.SetBatch()
    aplt.set_atlas_style()

    print(f"---->Working on the variable {cur_var}")

    for region, region_selections in signal_regions.items():
        df_region = df.Filter(region_selections)
        print(f"---->Working on the region {region}")

        fig, ax = aplt.subplots(1, 1)
        for process, samples in processes.items():

            sample_selection = " || ".join([f'sample=="{sample}"' for sample in samples])
            print(f"------>Working on the process: {process}, contains {samples}")
            rdf_sample = df_region.Filter(sample_selection)
            if cur_var.endswith("VBF") or cur_var.endswith("_ttjf") or cur_var.endswith("_ttj") or cur_var.endswith("_bbtt"):
              rdf_sample = rdf_sample.Filter("hasVBFJetCandidates==1")
            hist_name = f"{process}_{cur_var}"
            # Get the binning for the current variable
            binning = variable_binnings[cur_var]
            h = rdf_sample.Histo1D(ROOT.RDF.TH1DModel(hist_name, "", *binning), cur_var, "weight")
            hist = h.GetValue().Clone()
            if hist.Integral():
              hist.Scale(1.0 / hist.Integral())
              ax.plot(hist, label=process, labelfmt="L", linecolor=process_colors.get(process,ROOT.kBlack))

        ax.add_margins(top=0.30)
        # Set axis titles
        ax.set_xlabel(f"{cur_var}")
        ax.set_ylabel("Normalised to unity")
        # Add the ATLAS Label
        aplt.atlas_label(text="Internal", loc="upper left")
        ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
        ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau_{lep}#tau_{had}", size=22, align=13)
        ax.text(0.2, 0.74, region, size=22, align=13)
        if cur_var.endswith("VBF") or cur_var.endswith("_ttjf") or cur_var.endswith("_ttj") or cur_var.endswith("_bbtt"):
          ax.text(0.2, 0.69, "hasVBFJetCandidates==1", size=22, align=13)
        # Add legend
        ax.legend(loc=(0.55, 0.35, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))
        outName = os.path.join(outDir, f"{cur_var}_{region}_overlay_histogram.pdf")
        print(f"---->Saving {outName}")
        fig.savefig(outName)
        fig.savefig(outName.replace(".pdf",".eps"))
        fig.savefig(outName.replace(".pdf",".png")) 

if __name__ == '__main__':
    # Define the variables
    vars = []
    vars = ["SMBDT_ggF_inclusive", "SMBDT_ggF", "SMBDT_VBF", "SMBDT_ggF_VBF", "SMBDT_ggF_ADA"]
    #vars = []
    #vars += ["mHH","SMVBFBDTScore_V03_PCBT_extsh2211", "SMGGFBDTScore_test2_extsh2211", "SMGGFBDTScore_mHHsplit_test2_extsh2211"]
    #vars += ["mjjVBF", "dEtajjVBF", "cent_ttjf", "dRjjVBF", "multEtajjVBF", "mEff_ttjf", "fwm0_ttj"]
    #vars += ["dEtaHH", "Mb0lep", "hcm7_bbtt", "fwm4_bbtt", "dPtLepTau", "hcm3_ttjf", "mjjVBF", "SubLeadBjet", "TauPt", "dPhiLepMET", "pTSixObjVBF", "mHH", "mBB", "mMMC", "dRBB", "dRTauLep", "fwm2_ttj", "Mb1lep", "mHH_scaled", "LeadBjet"]
    #vars = ["mHH", "mBB", "mMMC", "dRBB", "dRTauLep", "n_jets", "HT", "T1", "T2", "ST", "MET", "MT2", "pTBB", "pTHH", "mTtau", "mTLep", "TauPt", "LepPt", "cosTheta", "pTTauLep", "LeadBjet", "SubLeadBjet", "dPhibbMET", "dPhiLepTauBB", "dPhiTauTauMET", "mHHStar", "dPhiLepMET", "dPhiTauMET", "cent_bbtt", "mEff_ttj", "fwm1_bbtt", "spher_bbtt", "pflow_bbtt", "mTW", "coshelicitybb", "coshelicitytautau", "mHH_scaled", "TauEta", "LepEta", "dRb0tau", "dRb1tau", "dRb0lep", "dRb1lep", "quant_b0", "quant_b1"]
    #vars += ["mHH","SMVBFBDTScore_V03","SMggFVBFBDTScore",
    #        "mBB","mMMC","mHH","mHHStar","mHH_scaled","dRTauLep","dPtLepTau","dRBB","dPhiLepTauBB","LeadBjet","SubLeadBjet","mTW","MET","METCent","dPhiLepMET","dPhiTauTauMET","HT","ST","MT2","pTBB","pTTauLep","pTTauTau","pTHH","minDRbl","minDRbtau","METSig","METSigPU","TauPt","TauEta","TauPhi","LepPt","LepEta","LepPhi","pTB0Uncorr","pTB1Uncorr","LeadJetPt","Mb0lep","Mb1lep","Mb0tau","Mb1tau","EtaBB","EtaTauTau","dPhiTauMET","dEtalepTau","LeadBjetEta","LeadBjetPhi","SubLeadBjetEta","SubLeadBjetPhi","METPhi","coshelicitybb","coshelicitytautau","cosTheta","dRb0tau","dRb1tau","dRb0lep","dRb1lep","dPhibbMET","dEtaBB","dEtaHH","dRHH","mTtau","mTLep","T1","T2",
    #        "mjjVBF","dRjjVBF","pTj1VBF","pTj2VBF","pTjjVBF","Etaj1VBF","Etaj2VBF","dEtajjVBF","dPhijjVBF","pTSixObjVBF","multEtajjVBF",
    #        "mEff_bbtt","cent_bbtt","apla_bbtt","spher_bbtt","fwm0_bbtt","fwm1_bbtt","fwm2_bbtt","fwm3_bbtt","fwm4_bbtt","hcm1_bbtt","hcm2_bbtt","hcm3_bbtt","hcm4_bbtt","hcm5_bbtt","hcm6_bbtt","hcm7_bbtt","hcm8_bbtt","plan_bbtt","varc_bbtt","vard_bbtt","circ_bbtt","pflow_bbtt","thrust_bbtt",
    #        "mEff_ttj","cent_ttj","apla_ttj","spher_ttj","fwm0_ttj","fwm1_ttj","fwm2_ttj","fwm3_ttj","fwm4_ttj","hcm1_ttj","hcm2_ttj","hcm3_ttj","hcm4_ttj","hcm5_ttj","hcm6_ttj","hcm7_ttj","hcm8_ttj","plan_ttj","varc_ttj","vard_ttj","circ_ttj","pflow_ttj","thrust_ttj",
    #        "mEff_ttjf","cent_ttjf","apla_ttjf","spher_ttjf","fwm0_ttjf","fwm1_ttjf","fwm2_ttjf","fwm3_ttjf","fwm4_ttjf","hcm1_ttjf","hcm2_ttjf","hcm3_ttjf","hcm4_ttjf","hcm5_ttjf","hcm6_ttjf","hcm7_ttjf","hcm8_ttjf","plan_ttjf","varc_ttjf","vard_ttjf","circ_ttjf","pflow_ttjf","thrust_ttjf"
    #        #"mHH", "mjjVBF", "fwm0_ttjf", "mEff_ttjf", "dEtaHH", "dPhiLepTauBB", "fwm0_ttj", "pTj1VBF", "hcm4_ttj", "pTHH", "mHHStar", "fwm0_bbtt", "MT2", "dEtaBB", "hcm2_ttjf", "hcm3_ttjf", "hcm1_ttjf", "dEtajjVBF", "multEtajjVBF", "pflow_bbtt",
    #]
    # Remove duplicates from the list by using a list comprehension and overwriting the original list
    vars = list(dict.fromkeys(vars))
    
    with Pool() as p:
        p.map(process_var, vars)

    print("Done!")