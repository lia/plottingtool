import ROOT
import os
import plotly.graph_objects as go
import numpy as np
import atlasplots as aplt

def apply_mva(rdf, h_dict):
  report = rdf.Report()
  count = rdf.Count()
  rdf = rdf.Filter("hasVBFJetCandidates == 1", "VBF region selection")
  rdf = rdf.Filter("OS == 1", "OS selection")
  rdf = rdf.Filter("mBB < 150000.", "low mBB selection") 
  rdf = rdf.Filter("n_btag == 2", "btag") 
  n_vars = 7
  n_fold = 3
  idx_str = ""
  for n in range(n_fold):
    xml_dir = os.path.join(storage,"xml", "all_in_one", f'SM_BDT_lephad_SMggF_SMVBF_V03_PCBT_fold_{n}.weights.xml')

    ROOT.gInterpreter.ProcessLine(f'''
        TMVA::Experimental::RReader model_{n} ("{xml_dir}");
        auto computeModel_{n} = TMVA::Experimental::Compute<{n_vars}, float>(model_{n});
    ''')
    var_names = ROOT.TMVA.Experimental.RReader(xml_dir).GetVariableNames()
    for var in var_names:
      rdf = rdf.Redefine(var, f'(float)({var})')

    rdf = rdf.Define(
      f"BDTG_{n}",
      eval(f'ROOT.computeModel_{n}'),
      eval(f'ROOT.model_{n}.GetVariableNames()'),
      )

    idx_str += f'( (event_number % {n_fold} == {n}) * BDTG_{n}.front() ) {"+" if n < n_fold - 1 else ""} '

  rdf = rdf.Define("BDTG", idx_str)

  h = {
    k: rdf.Filter(v).Histo1D((f"bdtg", ";BDTG;Yields", 20, -1.0, 1.0), "BDTG", "weight") for k, v in
    h_dict.items()
  }
  # h = rdf.Histo1D((f"bdtg", ";BDTG;Yields", 100, -1.0, 1.0), "BDTG", "weight")

  print(f"Total Entries: {count.GetValue()}")
  report.Print()

  return {k: v.GetPtr() for k, v in h.items()}

def make_new_point(h_vbf, k2v: float = 1.0):
  from reweight import calculate_coefficient
  k2v_dict = {
              'VBFSM': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 1, 1)),
              'VBFl0cvv0cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(0, 0, 1)),
              'VBFl10cvv1cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 10, 1)),
              'VBFl1cvv0p5cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(0.5, 1, 1)),
              'VBFl1cvv3cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(3, 1, 1)),
              'VBFl2cvv1cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 2, 1)),
              }

  str = "+".join([f"h_vbf['{k}'] * {k2v_dict[k]}" for k in h_vbf.keys()])
  # print(str)

  return eval(str)


def norm(h):
  return np.array([y for y in h])/ np.linalg.norm([y for y in h])

if __name__ == "__main__" :

  dir = os.getcwd()
  storage = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/" 
  inFolder = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/ntuple_VBF03/tree-PCBT-ggfvbfV03-extsh2211.root"
  rdf_ggf = ROOT.RDataFrame("Nominal", os.path.join(storage,"ntuple_VBF03", 'GGF.root'))
  rdf_vbf = ROOT.RDataFrame("Nominal", os.path.join(storage,"ntuple_VBF03", 'VBF.root'))
  
  h_ggf = apply_mva(rdf_ggf, {'ggF': f'sample == \"hhttbb\"'})['ggF']
  h_vbf = apply_mva(rdf_vbf, {
    'VBFSM': f'sample == \"hhttbbVBFSM\"',
    'VBFl0cvv0cv1': f'sample == \"hhttbbVBFl0cvv0cv1\"',
    'VBFl10cvv1cv1': f'sample == \"hhttbbVBFl10cvv1cv1\"',
    'VBFl1cvv0p5cv1': f'sample == \"hhttbbVBFl1cvv0p5cv1\"',
    'VBFl1cvv3cv1': f'sample == \"hhttbbVBFl1cvv3cv1\"',
    'VBFl2cvv1cv1': f'sample == \"hhttbbVBFl2cvv1cv1\"',
  }) 
  
  #setup style
  ROOT.gROOT.SetBatch()
  aplt.set_atlas_style()
  
  fig, ax = aplt.subplots(1, 1)

  h_ggf.Scale(1./h_ggf.Integral())
  h_vbf["VBFSM"].Scale(1./h_vbf["VBFSM"].Integral()) 
  ax.plot(h_ggf, label="hhttbb", linecolor = ROOT.kRed,  labelfmt="L")
  ax.plot(h_vbf["VBFSM"], label="hhttbbVBFSM", linecolor = ROOT.kBlue,  labelfmt="L") 

  ax.add_margins(top=0.30)
  # Set axis titles
  ax.set_xlabel("SMggFVBFScore_V03")
  ax.set_ylabel("Normalised to unity")
  # Add the ATLAS Label
  aplt.atlas_label(text="Internal", loc="upper left")
  ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
  ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau_{lep}#tau_{had}", size=22, align=13)
  # Add legend
  #ax.legend(loc=(0.50, 0.50, 0.95, 0.92))
  ax.legend(loc=(0.45, 0.55, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))
  fig.savefig(os.path.join("./",f"Standalone_BDT.pdf")) 
  
  exit(0) 
  x = [h_ggf.GetBinCenter(b + 1) for b in range(h_ggf.GetNbinsX())]

  colors = [
          'rgba(93, 164, 214, 0.85)', 'rgba(255, 144, 14, 0.85)', 'rgba(44, 160, 101, 0.85)',
          'rgba(255, 65, 54, 0.85)', 'rgba(207, 114, 255, 0.85)', 'rgba(127, 96, 0, 0.85)'
          ]

  fig = go.Figure()

  fig.add_trace(go.Bar(
    x=x,
    y=norm(h_ggf),
    width=0.1,
    marker_color='rgb(0, 45, 106)',
    marker_line_width=1.5,
    marker_line_color='rgb(0, 45, 106)',
    opacity=0.75,
    name=r"${\textrm{ggF}}$",
    legendgroup="SM",
  ))

  fig.add_trace(go.Bar(
    x=x,
    y=norm(h_vbf['VBFSM']),
    width=0.1,
    marker_color='firebrick',
    marker_line_width=1.5,
    marker_line_color='firebrick',
    opacity=0.75,
    name=r"${\textrm{VBF}}$",
    legendgroup="SM",
  ))

  for i, k2v in enumerate([-2, -1, 0, 1, 2, 3]):
    fig.add_trace(go.Scatter(
      x=x,
      y=norm(make_new_point(h_vbf, k2v)),
      mode='lines',
      name=f'$\kappa_{{2V}} = {k2v}$',
      line=dict(color=colors[i], width=2, ),
      opacity=0.95,
      legendgroup="BSM",
    ))

  # annotation
  x_base, y_base = 0.05, 0.97
  fig.add_annotation(
    text=r'<i><b>ATLAS<b><i>', showarrow=False, xref='paper', x=x_base, yref='paper', y=y_base,
    font=dict(size=30, family='Cambria'),
  )

  fig.add_annotation(
    text=r'Internal', showarrow=False, xref='paper', x=x_base + 0.16, yref='paper', y=y_base - 0.0037,
    font=dict(size=28, family='Cambria'),
  )


  y_base -= 0.092
  fig.add_annotation(
    text=r'$\large{b\bar{b}\tau_{lep}\tau_{had}}$', showarrow=False, xref='paper', x=x_base,
    yref='paper', y=y_base,
    font=dict(size=18, family='Arial'),
  )

  axis_attr = dict(
                linecolor="#666666", gridcolor='#F0F0F0', zerolinecolor='rgba(0,0,0,0)', linewidth=2, gridwidth=1,
                showline=True, mirror=True, tickfont=dict(size=16),
              )

  fig.update_xaxes(**axis_attr, showgrid=False, title=dict(text=r"ggF/VBF cat BDT score", font=dict(size=18)), )
  fig.update_yaxes(
    **axis_attr,
    showgrid=True,
    showexponent='all',
    exponentformat='power',
    title=dict(text=r"", font=dict(size=18)),
    range=[0, 0.6],
  )

  fig.update_layout(
    legend=dict(
      orientation="v",
      yanchor="top",
      y=0.97,
      xanchor="right",
      x=0.8,
      font=dict(size=14),
      # itemwidth=40,
    ),
    width=800,
    height=700,
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
  )

  fig.show()

  fig.write_image("ggFVBF_BDT_k2V_test.pdf")


  






