import ROOT

# Input and output ROOT file names
input_file_name = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/Reader_EBR_V02/hist-ALL-extsh2211.root"
output_file_name = f"./converted.root"

# The TDirectory in the input ROOT file containing histograms
input_tdirectory_name = "FitInputs"

# The TDirectory in the output ROOT file to save histograms
output_tdirectory_name = "BDTScorePreselection"
old_suffixes = [#"SMVBFBDTScore_V03_PCBT_extsh2211_2000",
                "SMGGFBDTScore_test2_extsh2211_2000",
                "SMGGFBDTScore_mHHsplit_test2_extsh2211_2000"]
new_suffix = "SMBDT"

processes = ["hhttbb",
             'hhttbbKLRW0p0',
             "hhttbbKLRW20p0",
             "hhttbbVBFSM",
             "hhttbbVBFl1cvv3cv1",
             "hhttbbVBFl1cvv0p5cv1", 
             "hhttbbVBFl2cvv1cv1", 
             "hhttbbVBFl0cvv0cv1", 
             "hhttbbVBFl10cvv1cv1"]

middle = ["2tag_350mHH_OS_GGFSR",
          "2tag_0_350mHH_OS_GGFSR",
          "2tag_350mHH_OS_VBFSR",
          "2tag_0_350mHH_OS_VBFSR"]
middle_to_suffix = {
    "2tag_350mHH_OS_GGFSR": "SMGGFBDTScore_test2_extsh2211_2000",
    "2tag_0_350mHH_OS_GGFSR": "SMGGFBDTScore_test2_extsh2211_2000",
    "2tag_350mHH_OS_VBFSR": "SMGGFBDTScore_mHHsplit_test2_extsh2211_2000",
    "2tag_0_350mHH_OS_VBFSR": "SMGGFBDTScore_mHHsplit_test2_extsh2211_2000"
}

# Function to change the name of a histogram
def change_histogram_name(old_name):
    # Replace the old suffix with the new suffix
    for old_suffix in old_suffixes:
        if old_name.endswith(old_suffix):
            return old_name.replace(old_suffix, new_suffix)
    # Return the old name if none of the old suffixes match
    return old_name

def combine_histograms(hist1, hist2):
    combined_hist = hist1.Clone()
    combined_hist.Add(hist2)
    return combined_hist

# Generate the list of histogram names to process
histograms_to_process = []

for process in processes:
    for mid in middle:
        old_suffix = middle_to_suffix[mid]
        histograms_to_process.append(f"{process}_{mid}_{old_suffix}")
# Open the input ROOT file
print(" -> Opening input ROOT file...")
input_file = ROOT.TFile.Open(input_file_name)

# Get the input TDirectory
input_directory = input_file.Get(input_tdirectory_name)

# Create a new ROOT file for output
print(" -> Creating output ROOT file...")
output_file = ROOT.TFile(output_file_name, "RECREATE")

# Create the output TDirectory
print(" -> Creating output TDirectory...")
output_directory = output_file.mkdir(output_tdirectory_name)

# Iterate through the list of histogram names to process
for hist_name in histograms_to_process:
    # Check if the histogram exists in the input TDirectory
    histogram = input_directory.Get(hist_name)
    if not histogram:
        print(f"{hist_name} not found")
        continue

    # Read the histogram
    print(f" --> Reading histogram: {hist_name}")

    # Change the histogram name
    print(f" --> Changing histogram name: {histogram.GetName()}")
    new_name = change_histogram_name(histogram.GetName())
    print(f" --> new name: {new_name}")
    # Save the histogram to the output TDirectory
    print(f" ---> Saving histogram: {new_name}")
    output_directory.cd()
    histogram.SetName(new_name)
    histogram.Write()

output_file.Close()
input_file.Close()

print(f"Processed histograms saved to {output_file_name} in the TDirectory '{output_tdirectory_name}'")
