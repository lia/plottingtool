import ROOT
#from helper import ofufbins, create_default_legend, calculate_separation_index

#path = '/afs/cern.ch/work/l/lia/public/bbtautau/K2VOptimization/Training/tree-mc16ade-SM-SLT-ALL_APC_V13.root'
path = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V16.root'
histos = {}
Yields = {}
histosVBFSR = {}
YieldsVBFSR = {}
K2V=['hhttbbVBFSM','hhttbbVBFl1cvv0cv1','hhttbbVBFl1cvv0p5cv1','hhttbbVBFl1cvv1p5cv1','hhttbbVBFl1cvv2cv1','hhttbbVBFl1cvv3cv1','hhttbb']
df = ROOT.RDataFrame("Nominal", path)
df = df.Filter('mBB < 150000.')
df = df.Filter('OS==1')
df = df.Filter('!sample.empty()')
df = df.Filter('mHH>0.')
df = df.Define('MHH','mHH/1000.')
#var = "SMggFVBFBDTScore"
var = "SMVBFBDTScore"
for ch in K2V:
  print(ch)
  df_tmp = df
  df_tmp = df_tmp.Filter('(sample == \"{}\")'.format(ch))
  h = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{var}_{ch}", f"{var}", 200, -1, 1), f"{var}", "weight"))
  Yields[ch] = df_tmp.Sum('weight').GetValue()
  histos[ch] = h.GetValue().Clone()
  histos[ch].Scale(1./Yields[ch])
  df_tmp = df_tmp.Filter('region==1')
  h2 = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{var}_VBFSR_{ch}", f"{var}", 200, -1, 1), f"{var}", "weight"))
  YieldsVBFSR[ch] = df_tmp.Sum('weight').GetValue()
  histosVBFSR[ch] = h2.GetValue().Clone()
  histosVBFSR[ch].Scale(1./YieldsVBFSR[ch])
#ROOT.RDF.RunGraphs([histos[s] for s in K2V])
#ROOT.RDF.RunGraphs([histosVBFSR[s] for s in K2V])
#Nomalise to Unity
# Create the plot

# Set styles
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetStyle("ATLAS")
  
# Create canvas with pads for main plot and data/MC ratio
c = ROOT.TCanvas("c", "", 800, 600)
pad = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
pad.SetTickx(False)
pad.SetTicky(False)
pad.Draw()
pad.cd() 
    
# Draw histos
for ch, color in zip(histos, [(255, 0, 0), (208, 240, 193), (195, 138, 145), (155, 152, 204), (248, 206, 104),(120,0,204),(0, 255, 0),(255, 255, 0),(0, 0, 255),(255, 0, 255) ,(0, 255, 255),(128, 128, 123)]):
  histos[ch].SetLineWidth(1)
  histos[ch].SetLineColor(ROOT.TColor.GetColor(*color))
  histos[ch].Draw("histo same")
histos['hhttbbVBFSM'].GetXaxis().SetTitle(f"{var}")
histos['hhttbbVBFSM'].GetYaxis().SetTitle("Normalised to 1")
histos['hhttbbVBFSM'].GetYaxis().SetLabelSize(0.04)
histos['hhttbbVBFSM'].GetYaxis().SetTitleSize(0.045)
histos['hhttbbVBFSM'].GetXaxis().SetLabelSize(0.04)
histos['hhttbbVBFSM'].GetXaxis().SetTitleSize(0.045)
histos['hhttbbVBFSM'].SetMinimum(0)
histos['hhttbbVBFSM'].SetMaximum(0.35)
histos['hhttbbVBFSM'].GetYaxis().ChangeLabel(1, -1, 0)

# Add legend
legend = ROOT.TLegend(0.60, 0.65, 0.92, 0.92)
legend.SetTextFont(42)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
legend.SetTextSize(0.02)
legend.SetTextAlign(32)
for ch in histos:
  legend.AddEntry(histos[ch], '{}\tYields: {:.2f}'.format(ch,Yields[ch]) ,"f")
legend.Draw("SAME")

# Add ATLAS label
text = ROOT.TLatex()
text.SetNDC()
text.SetTextFont(72)
text.SetTextSize(0.045)
text.DrawLatex(0.21, 0.86, "ATLAS")
#text.DrawLatex(0.79, 0.14, "ATLAS Internal")
text.SetTextFont(42)
text.DrawLatex(0.21 + 0.16, 0.86, "Internal")
text.SetTextSize(0.04)
text.DrawLatex(0.21, 0.80, "#sqrt{s} = 13 TeV, Simulation")
text.DrawLatex(0.21, 0.74, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-}")
c.SaveAs(f"K2V_Compare_{var}.png")
print("Saved figure to K2VCompare.png")

# Create canvas with pads for main plot and data/MC ratio
c = ROOT.TCanvas("c", "", 800, 600)
pad = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
pad.SetTickx(False)
pad.SetTicky(False)
pad.Draw()
pad.cd() 
    
# Draw histos
for ch, color in zip(histosVBFSR, [(255, 0, 0), (208, 240, 193), (195, 138, 145), (155, 152, 204), (248, 206, 104),(120,0,204),(0, 255, 0),(255, 255, 0),(0, 0, 255),(255, 0, 255) ,(0, 255, 255),(128, 128, 123)]):
  histosVBFSR[ch].SetLineWidth(1)
  histosVBFSR[ch].SetLineColor(ROOT.TColor.GetColor(*color))
  histosVBFSR[ch].Draw("histo same")
histosVBFSR['hhttbbVBFSM'].GetXaxis().SetTitle(f"{var}")
histosVBFSR['hhttbbVBFSM'].GetYaxis().SetTitle("Normalised to 1")
histosVBFSR['hhttbbVBFSM'].GetYaxis().SetLabelSize(0.04)
histosVBFSR['hhttbbVBFSM'].GetYaxis().SetTitleSize(0.045)
histosVBFSR['hhttbbVBFSM'].GetXaxis().SetLabelSize(0.04)
histosVBFSR['hhttbbVBFSM'].GetXaxis().SetTitleSize(0.045)
histosVBFSR['hhttbbVBFSM'].SetMinimum(0)
histosVBFSR['hhttbbVBFSM'].SetMaximum(0.35)
histosVBFSR['hhttbbVBFSM'].GetYaxis().ChangeLabel(1, -1, 0)

# Add legend
legend = ROOT.TLegend(0.60, 0.65, 0.92, 0.92)
legend.SetTextFont(42)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
legend.SetTextSize(0.02)
legend.SetTextAlign(32)
for ch in histos:
  legend.AddEntry(histosVBFSR[ch], '{}\tYields: {:.2f}'.format(ch,YieldsVBFSR[ch]) ,"f")
legend.Draw("SAME")

# Add ATLAS label
text = ROOT.TLatex()
text.SetNDC()
text.SetTextFont(72)
text.SetTextSize(0.045)
text.DrawLatex(0.21, 0.86, "ATLAS")
text.SetTextFont(42)
text.DrawLatex(0.21 + 0.16, 0.86, "Internal")
text.SetTextSize(0.04)
text.DrawLatex(0.21, 0.80, "#sqrt{s} = 13 TeV"+" Simulation")
text.DrawLatex(0.21, 0.74, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-} VBF Cat.")
c.SaveAs(f"K2V_VBFSR_Compare_{var}.png")
print("Saved figure to K2VCompare.png")



