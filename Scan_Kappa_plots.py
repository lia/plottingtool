import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import re
import os
from datetime import date
from tabulate import tabulate
import subprocess

def create_table(labels, dir_path):
    header = ['$WP$', '$\mu_{HH}$', '$\mu_{ggF}$', '$\mu_{VBF}$', '$\kappa_{\lambda}$', '$\kappa_{2V}$']
    table = []

    labels, ci_68_KL, ci_95_KL      = read_kappa_limits('KL')
    labels, ci_68_K2V, ci_95_K2V    = read_kappa_limits('K2V')
    mu_ggF = read_mu_limits('muggF', labels)[:, 0]
    mu_VBF = read_mu_limits('muVBF', labels)[:, 0]
    mu_HH = read_mu_limits('muHH', labels)[:, 0]

    for i in range(len(labels)):
        row = [labels[i], round(mu_HH[i], 2), round(mu_ggF[i], 2), round(mu_VBF[i], 2), 
               f"{round(ci_95_KL[i][0], 2)}, {round(ci_95_KL[i][1], 2)}", 
               f"{round(ci_95_K2V[i][0], 2)}, {round(ci_95_K2V[i][1], 2)}"]
        table.append(row)

    latex_table = tabulate(table, headers=header, tablefmt='latex_longtable')

    latex_doc = "\\documentclass{article}\n\\usepackage{longtable}\n\\begin{document}\n" + latex_table + "\n\\end{document}"

    with open(f'{dir_path}/table.tex', 'w') as file:
        file.write(latex_doc)

    subprocess.run(['pdflatex', '-output-directory', dir_path, f'{dir_path}/table.tex'])

def read_mu_limits(mu_type, lables):
    
    mu_limits = []
    for lable in lables:
        label = lable.replace('.', 'p').replace('-', 'm')
        if label.endswith('0'):
            label = label[:-1]
        WP = label
        file_path = f'/sps/atlas/a/angli/ATLAS/bbtautau/WSMaker_HH_bbtautau_EB/output/HH230525_EB.SCAN_WP_{WP}_{mu_type}_HH_13TeV_SCAN_WP_{WP}_{mu_type}_{mu_type}_FloatOnly_lephad_BDT_0/logs/limit.log'

        with open(file_path, 'r') as f:
            lines = f.readlines()

        for line in lines:
            if 'Expected limit' in line:
                # Get the limit values
                limit_values = re.findall(r'[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?', line)
                mu_limits.append(list(map(float, limit_values)))

    return np.array(mu_limits)

def plot_mu_limits(mu_limits, labels, mu_type, dir_path):
    plt.figure(figsize=(50, 5))
    
    best_fits = mu_limits[:, 0]
    
    # find the minimum point and its label
    min_point = np.min(best_fits)
    min_label = labels[np.argmin(best_fits)]
    
    indices = np.arange(len(labels))
    plt.plot(indices, best_fits, 'o-')

    plt.xticks(ticks=indices, labels=labels, rotation='vertical')
    plt.title(f'Mu {mu_type} Limits')
    plt.xlabel('WP Value')
    plt.ylabel('Expected limit')
    
    # include the minimum point and its label in the legend
    legend_label = f'Minimum value: {min_point:.2f}, WP: {min_label}'
    plt.legend([legend_label])
    
    plt.tight_layout()
    
    # save plot
    for ext in ['pdf', 'png']:
        plt.savefig(f'{dir_path}/{mu_type}_limits.{ext}')
    
    plt.close()


def read_kappa_limits(kappa):
    file_path = f'{kappa}.txt'  # construct the file path

    labels = []
    ci_68 = []
    ci_95 = []
    with open(file_path, 'r') as f:
        lines = f.readlines()

    for i in range(len(lines)):
        line = lines[i]
        if 'for' in line:
            label = line.split('for ')[1].strip()
            if 'm' in label or 'p' in label:
                label = label.replace('p', '.').replace('m', '-')
                label = f"{float(label):.2f}"  # format the number to have two decimal places
            labels.append(label)
        elif '68% CI' in line:
            ci_68.append(list(map(float, re.findall(r'array\(\[(.*?)(?:,)?\]\)', lines[i + 1])[0].split(','))))
        elif '95% CI' in line:
            ci_95.append(list(map(float, re.findall(r'array\(\[(.*?)(?:,)?\]\)', lines[i + 1])[0].split(','))))
    return labels, np.array(ci_68), np.array(ci_95)


def plot_limits(indices, labels, ci_68, ci_95, ci_to_plot, kappa, dir_path):
    plt.figure(figsize=(50, 5))  # increase width of the figure
    # plot the limits
    # Verify the lengths of indices, mean_95, and error_95
    if ci_to_plot in ['95%', 'both']:
        mean_95 = (ci_95[:, 0] + ci_95[:, 1]) / 2
        error_95 = (ci_95[:, 1] - ci_95[:, 0]) / 2
        plt.errorbar(indices, mean_95, yerr=error_95, fmt='o', color='blue', label='95% CI', alpha=0.5)
    if ci_to_plot in ['68%', 'both']:
        mean_68 = (ci_68[:, 0] + ci_68[:, 1]) / 2
        error_68 = (ci_68[:, 1] - ci_68[:, 0]) / 2
        plt.errorbar(indices, mean_68, yerr=error_68, fmt='o', color='red', label='68% CI')

    plt.xticks(ticks=indices, labels=labels, rotation='vertical')
    plt.title('#kappa_{#lambda} Limits' if kappa == 'KL' else '#kappa_{2V} Limits')
    plt.xlabel('ggF/VBF BDT WP')  # set x-axis label
    plt.ylabel('Value')
    plt.legend()
    plt.tight_layout()

    # save plot
    for ext in ['pdf', 'png']:
        plt.savefig(f'{dir_path}/{kappa}_limits.{ext}')

    plt.close()

def plot_differences(indices, labels, ci_68, ci_95, ci_to_plot, kappa, dir_path):
    plt.figure(figsize=(50, 5))
    # calculate all differences
    diff_68 = ci_68[:, 1] - ci_68[:, 0]
    diff_95 = ci_95[:, 1] - ci_95[:, 0]

    # Find the minimum difference and its corresponding label
    min_diff = np.min(diff_68) if ci_to_plot == '68%' else np.min(diff_95)
    min_label = labels[np.argmin(diff_68)] if ci_to_plot == '68%' else labels[np.argmin(diff_95)]
    # Find the lower and upper limits corresponding to the min_diff value
    lower_limit = ci_68[np.argmin(diff_68)][0] if ci_to_plot == '68%' else ci_95[np.argmin(diff_95)][0]
    upper_limit = ci_68[np.argmin(diff_68)][1] if ci_to_plot == '68%' else ci_95[np.argmin(diff_95)][1]

    # find the overall minimum and maximum difference
    if ci_to_plot in ['both']:
        min_diff = min(diff_68.min(), diff_95.min())
        max_diff = max(diff_68.max(), diff_95.max())
    elif ci_to_plot in ['68%']:
        min_diff = diff_68.min()
        max_diff = diff_68.max()
    elif ci_to_plot in ['95%']:
        min_diff = diff_95.min()
        max_diff = diff_95.max()

    # add some padding for the plot (5% of the range)
    padding = (max_diff - min_diff) * 0.05
    ymin, ymax = min_diff - padding, max_diff + padding

    # plot the differences
    if ci_to_plot in ['95%', 'both']:
        plt.plot(indices, diff_95, 'o-', color='blue', label='95% CI')
    if ci_to_plot in ['68%', 'both']:
        plt.plot(indices, diff_68, 'o-', color='red', label='68% CI')

    plt.ylim([ymin, ymax])  # set the y-axis range
    plt.xticks(ticks=indices, labels=labels, rotation='vertical')
    # Add the minimum value and label to the legend
    legend_label = f'BEST WP is {min_label}, Max-Min = {min_diff:.2f}, [{lower_limit:.2f}, {upper_limit:.2f}]]'
    plt.plot([], [], ' ', label=legend_label)
    plt.legend(fontsize='large')
    plt.title('Difference between upper and lower limits')
    plt.xlabel('ggF/VBF BDT WP')
    plt.ylabel('Difference')
    plt.legend()
    plt.tight_layout()

    # save plot
    for ext in ['pdf', 'png']:
        plt.savefig(f'{dir_path}/{kappa}_differences.{ext}')

    plt.close()

def MakeWPSanPlots():
    # get current date
    current_date = date.today().isoformat()  # YYYY-MM-DD format

    # directory to save the plot
    dir_path = f'/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool_outdir/Scan_{current_date}'

    # create directory if it doesn't exist
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    
    kappa_types = ['KL', 'K2V']  # add more kappa types if needed
    ci_to_plot = '95%'  # can take values '68%', '95%', or 'both'

    for kappa in kappa_types:
        labels, ci_68, ci_95 = read_kappa_limits(kappa)
        indices = np.arange(len(labels))

        plot_limits(indices, labels, ci_68, ci_95, ci_to_plot, kappa, dir_path)
        plot_differences(indices, labels, ci_68, ci_95, ci_to_plot, kappa, dir_path)

    mu_types = ['muggF','muVBF','muHH']
    for mu_type in mu_types:
        mu_limits = read_mu_limits(mu_type, labels)
        plot_mu_limits(mu_limits, labels, mu_type, dir_path)

    create_table(labels, dir_path)
    print("Created table in Latex format and saved it as PDF.")

if __name__ == '__main__':
    MakeWPSanPlots()

