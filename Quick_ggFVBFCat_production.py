import os
import sys
import ROOT

BDT='SMggFVBFBDTScore'
#processes to save
Processes = ["hhttbbKLRW0p0","hhttbbKLRW20p0","data","ggFHtautau","hhttbb","DY","stopWt","stops","stopt","ttbar","ttbarFake","ttH","ttW","ttZ","hhttbbVBFl1cvv1cv1p5","hhttbbVBFl1cvv0p5cv1","hhttbbVBFl0cvv0cv1","hhttbbVBFl10cvv1cv1","hhttbbVBFl0cvv1cv1","hhttbbVBFl1cvv1cv0p5","hhttbbVBFl1cvv2cv1","hhttbbVBFl2cvv1cv1","hhttbbVBFl1cvv0cv1","hhttbbVBFl1cvv1p5cv1","hhttbbVBFSM","hhttbbVBFl1cvv3cv1","VBFHtautau","ggZHtautau","WHtautau","ggZHbb","qqZHbb","WHbb","qZHbb","W","WFake","WZ","WW","Wtt","WttFake","Zbc","Zbb","Zbl","Zcc","Zcl","Zl","ZZ","Zttbb","Zttbc","Zttbl","Zttl","Zttcl","Zttcc","DYtt"]
#Processes = ['hhttbbVBFSM']
BDT_Names = ['SMVBFBDTScore']
# Select events for the analysis
ROOT.gInterpreter.Declare("""
  int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
    int m_region = -1;
    if(!hasVBFJetCandidates){
      m_region = 0;
    }else{
      if(SMBDTggFVBF > cut ){
        m_region = 0;
      }else{
        m_region = 1;
      }
    }
    return m_region;
  }
  """)


########################
#Quick Projection:
########################
if __name__ == "__main__" :
  inFile = sys.argv[1] if len(sys.argv) > 1 else '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V16.root'
  outFile = sys.argv[2] if len(sys.argv) > 2 else inFile.replace('.root', ".slim.root")
  cut = sys.argv[3] if len(sys.argv) > 3 else -0.13
  
  Hist = {}
  df = ROOT.RDataFrame("Nominal", inFile)
  df = df.Filter("n_btag==2")
  df = df.Filter("OS==1")
  df = df.Filter('mBB < 150000.')
  suffix = "_".join(["2tag","0mHH","OS"])
  for cur_proc in Processes:
    print(f'-->Working on {cur_proc}')
    df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    for cur_BDT in BDT_Names:
      df_ggF_like = df_tmp.Filter(f"Category(hasVBFJetCandidates,{BDT},{cut}) == 0")
      name = "_".join([cur_proc,suffix,"GGFSR",cur_BDT])
      h = (df_ggF_like.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{cur_BDT}", "weight"))
      Hist[name] = h.GetValue().Clone()
      df_VBF_like = df_tmp.Filter(f"Category(hasVBFJetCandidates,{BDT},{cut}) == 1") 
      name = "_".join([cur_proc,suffix,"VBFSR",cur_BDT])
      h = (df_VBF_like.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{cur_BDT}", "weight"))
      Hist[name] = h.GetValue().Clone() 
  fout = ROOT.TFile.Open(outFile, "RECREATE")
  for cur_hist in Hist:
    Hist[cur_hist].Write()

  fout.Close()


