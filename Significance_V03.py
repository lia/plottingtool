import os
from datetime import datetime
import numpy as np
import ROOT
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def significance(signal, background, mode = "default"):
    if background > 0:
        if mode == "default":
            return signal / np.sqrt(signal + background)
    else:
        return 0

def efficiency(signal, Total):
    if Total > 0:
        return signal / Total 
    else:
        return 0 



def calculate_data(rdf, WP_values, signal_regions, processes):
    data = []

    #Total = {}
    #Total["VBFSR"]         = rdf.Filter('sample=="hhttbbVBFSM"&&hasVBFJetCandidates==1').Sum("weight").GetValue()
    #Total["low-mHH-GGFSR"]  = rdf.Filter('sample=="hhttbb"&&mHH<350000&&hasVBFJetCandidates==1').Sum("weight").GetValue()
    #Total["high-mHH-GGFSR"] = rdf.Filter('sample=="hhttbb"&&mHH>350000&&hasVBFJetCandidates==1').Sum("weight").GetValue() 
    
    #print(f"-->Total Yields of VBF : {Total['VBFSR']}")
    #print(f"-->Total Yields of mHH-low ggF : {Total['low-mHH-GGFSR']}")
    #print(f"-->Total Yields of mHH-high ggF : {Total['high-mHH-GGFSR']}") 
    #print(f"-->Total Yields of ggF : {Total['low-mHH-GGFSR']+Total['high-mHH-GGFSR']}")  
    
    for WP in WP_values:
        print(f"-->Working on WP: {WP}")
        for region_name, region_selection in signal_regions.items():
            print(f"---->Working on region: {region_name}")
            Total = {}
            Total["VBF"] = rdf.Filter('sample=="hhttbbVBFSM"').Sum("weight").GetValue()
            Total["low-mHH-ggF"] = rdf.Filter('sample=="hhttbb"&&mHH<350000').Sum("weight").GetValue()
            Total["high-mHH-ggF"] = rdf.Filter('sample=="hhttbb"&&mHH>350000').Sum("weight").GetValue() 
            
            print(f"-->Total Yields of VBF : {Total['VBFSR']}")
            print(f"-->Total Yields of mHH-low ggF : {Total['low-mHH-GGFSR']}")
            print(f"-->Total Yields of mHH-high ggF : {Total['high-mHH-GGFSR']}") 
            print(f"-->Total Yields of ggF : {Total['low-mHH-GGFSR']+Total['high-mHH-GGFSR']}")  
            
            region_selection_formatted = region_selection.format(BDT="SMggFVBFBDTScore_V03", WP=str(WP))
            rdf_region = rdf.Filter(region_selection_formatted)

            signal_mHHlow_ggF  = 0
            signal_mHHhigh_ggF = 0 
            signal_VBFSM = 0
            background = 0
            
            for process, samples in processes.items():
                print(f"------>Working on process: {process}")
                sample_selection = " || ".join([f'sample=="{sample}"' for sample in samples])
                rdf_sample = rdf_region.Filter(sample_selection)
                weighted_yield = rdf_sample.Sum("weight").GetValue()

                
                if process not in ["ggFKL10"]:
                    if process == "ggF" and "GGFSR" in region_name:
                        signal_mHHlow_ggF  += rdf_sample.Filter("mHH<350000").Sum("weight").GetValue()
                        signal_mHHhigh_ggF += rdf_sample.Filter("mHH>350000").Sum("weight").GetValue() 
                    elif process == "VBFSM":
                        signal_VBFSM += rdf_sample.Sum("weight").GetValue()
                    else:
                        background += rdf_sample.Sum("weight").GetValue() 

            significance_ggF_bkg = significance(signal_mHHlow_ggF, background)
            significance_VBF_bkg = significance(signal_mHHhigh_ggF, background)
            significance_ggF_VBF = significance(signal_ggF, signal_VBFSM) 
            significance_VBF_ggF = significance(signal_VBFSM, signal_VBFSM)

            efficiency_mHHlow_ggF   = efficiency(signal_ggF, Total["low-mHH-ggF"])
            efficiency_mHHhigh_ggF  = efficiency(signal_ggF, Total["high-mHH-ggF"]) 
            efficiency_VBF   = efficiency(signal_VBFSM, Total["VBF"])

     
            efficiency_VBF_ggF   = efficiency(signal_VBFSM, signal_VBFSM)
            
            significance_total = significance(signal,background)
            signigicance_local = significance()
            data.append({"WP": WP, 
                         "Region": region_name, 
                         "Significance_ggF_bkg": significance_ggF_bkg,
                         "Significance_VBF_bkg": significance_VBF_bkg,
                         "Significance_ggF_VBF": significance_ggF_VBF,
                         "Significance_VBF_ggF": significance_VBF_ggF,
                         "Efficiency_ggF_bkg":   efficiency_ggF_bkg, 
                         "Efficiency_VBF_bkg":   efficiency_VBF_bkg, 
                         "Efficiency_ggF_VBF":   efficiency_ggF_VBF, 
                         "Efficiency_VBF_ggF":   efficiency_VBF_ggF, 
                         })
    exit(0)
    return data

def plot(data,region_name,plot_name):

    
    #data[data["Region" == region_name]]["WP"].shape,data[data["Region" == region_name]][plot_name].shape,)
    plt.figure()  # Create a new figure for the significance plot
    print("WP_values shape:", WP_values.shape)
    print("Filtered data shape:", data[(data["Region"] == region_name) & (data["Efficiency_ggF"] >= 0)]["Significance"].shape)
    plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_ggF"] >= 0)]["Significance"], label="ggF")
    plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_VBFSM"] >= 0)]["Significance"], label="VBFSM")
    plt.xlabel("WP_values")
    plt.ylabel("Significance")
    plt.legend()
    plt.savefig(os.path.join(outDir,"significance_vs_WP_values.pdf"))  # Save the significance plot

def create_plots(data, signal_regions, outDir):
    print("-->Creating significance and efficiency plots")
    #plt.figure(figsize=(20, 10))
    WP_values = data["WP"]
    Region_plots = {"low-mHH-GGFSR":    ["Significance_ggF_bkg","Significance_ggF_VBF", "Efficiency_ggF_bkg", "Efficiency_ggF_VBF"],
                    "high-mHH-GGFSR":   ["Significance_ggF_bkg","Significance_ggF_VBF", "Efficiency_ggF_bkg", "Efficiency_ggF_VBF"],
                    "VBFSR":            ["Significance_VBF_bkg","Significance_VBF_ggF", "Efficiency_VBF_bkg", "Efficiency_VBF_ggF"],
                    }
    for i, region_name in enumerate(signal_regions):
        for plot_name in Region_plots[signal_regions]:
            plot(data,region_name,plot_name)
        # Significance vs. WP_values plot
        plot(data["WP"],Region_plots[region_name],region_name)
        plt.figure()  # Create a new figure for the significance plot
        print("WP_values shape:", WP_values.shape)
        print("Filtered data shape:", data[(data["Region"] == region_name) & (data["Efficiency_ggF"] >= 0)]["Significance"].shape)
        plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_ggF"] >= 0)]["Significance"], label="ggF")
        plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_VBFSM"] >= 0)]["Significance"], label="VBFSM")
        plt.xlabel("WP_values")
        plt.ylabel("Significance")
        plt.legend()
        plt.savefig(os.path.join(outDir,"significance_vs_WP_values.pdf"))  # Save the significance plot

        # Efficiencies vs. WP_values plot
        plt.figure()  # Create a new figure for the efficiency plot
        plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_ggF"] >= 0)]["Efficiency_ggF"], label="ggF")
        plt.plot(WP_values, data[(data["Region"] == region_name) & (data["Efficiency_VBFSM"] >= 0)]["Efficiency_VBFSM"], label="VBFSM")
        plt.xlabel("WP_values")
        plt.ylabel("Efficiency")
        plt.legend()
        plt.savefig(os.path.join(outDir,"efficiency_vs_WP_values.png"))  # Save the efficiency plot


def plot_data(x_data, y_data, x_label, y_label, region_names, title, output_file_path):
    plt.figure(figsize=(10, 5))
    for region_name in region_names:
        plt.plot(x_data[region_name], y_data[region_name], label=region_name)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend()
    plt.title(title)
    plt.savefig(output_file_path)

def create_plots(data, WP_values, signal_regions, outDir):
    print("-->Creating significance and efficiency plots")
    region_names = list(signal_regions.keys())

    # Significance plot
    x_data = {region: WP_values for region in region_names}
    y_data = {region: data[data["Region"] == region]["Significance"] for region in region_names}
    plot_data(x_data, y_data, "WP", "Significance", region_names, "Significance vs WP", os.path.join(outDir, "Significance_Efficiency_Plots.png"))

    # Efficiency plot
    y_data = {region: data[data["Region"] == region]["Efficiency"] for region in region_names}
    plot_data(x_data, y_data, "WP", "Efficiency", region_names, "Efficiency vs WP", os.path.join(outDir, "Efficiency_Efficiency_Plots.png"))

    # ggF vs VBFSM plot
    x_data = {region: data[data["Region"] == region]["ggF_yield"] for region in region_names}
    y_data = {region: data[data["Region"] == region]["VBFSM_yield"] for region in region_names}
    for region_name in region_names:
        plt.figure(figsize=(10, 5))
        plt.plot(x_data[region_name], y_data[region_name], label="Background")
        plt.scatter(x_data[region_name], y_data[region_name], s=50, c="red", label="ggF")
        plt.xlabel("ggF Yield")
        plt.ylabel("VBFSM Yield")
        plt.legend()
        plt.title(f"ggF vs VBFSM Yield in {region_name}")
        output_file_path = os.path.join(outDir, f"ggF_vs_VBFSM_Yield_{region_name}.png")
        plt.savefig(output_file_path)

def main():
    InPut = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/Reader_EBR_V02/tree-extsh2211.root"
    rdf = ROOT.RDataFrame("Nominal", InPut)

    # Add preselection
    PreSelection = "OS==1&&n_btag==2&&mBB<150000&&hasVBFJetCandidates==1"
    rdf = rdf.Filter(PreSelection)

    # Define your processes, signal regions, and category here
    processes = {
      "ggF": ['hhttbb'],
      "VBFSM": ['hhttbbVBFSM'],
      "Diboson": ["WZ", "ZZ", "WW"],
      "Zttjets": ["Zttbb", "Zttbl", "Zttbc", "Zttl", "Zttcc", "Zttcl"],
      "Zjets": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc"],
      "Stop": ["stops", "stopt", "stopWt"],
      "ttV": ["ttW", "ttZ"],
      #"DY": ["DY", "DYtt"],
      "SHiggs": ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau"],
      "ttbar": ["ttbar"],
      "Others": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc", "Zl", "W", "W", "Wtt","DY", "DYtt"],
      "Fakes": ["WFake", "WttFake", "ttbarFake"],
    }

    signal_regions = {
      "low-mHH-GGFSR":    f"mHH<350000&&Category(hasVBFJetCandidates,{{BDT}},{{WP}})==0",
      "high-mHH-GGFSR":   f"mHH>350000&&Category(hasVBFJetCandidates,{{BDT}},{{WP}})==0",
      "VBFSR":            f"Category(hasVBFJetCandidates,{{BDT}},{{WP}})==1",
    }
    ROOT.gInterpreter.Declare("""
    int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
      int m_region = -1;
      if(!hasVBFJetCandidates){
        m_region = 0;
      }else{
        if(SMBDTggFVBF > cut ){
          m_region = 0;
        }else{
          m_region = 1;
        }
      }
      return m_region;
    }
    """)
    # Add an option to choose the number of WP points for the calculation
    num_WP_points = 5  # Set the desired number of WP points
    WP_values = np.linspace(-1, 1, num_WP_points)

    print("-->Calculating data")
    data = calculate_data(rdf, WP_values, signal_regions, processes)
    df = pd.DataFrame(data)

    outDir = f"./Significance_{datetime.now().strftime('%Y%m%d')}"
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    df.to_csv(os.path.join(outDir, "Significance_Efficiency_Data.csv"), index=False)

    print("-->Creating plots")
    create_significance_efficiency_plots(df, WP_values, signal_regions, outDir)
    print("Done!")

if __name__ == "__main__":
    main()


