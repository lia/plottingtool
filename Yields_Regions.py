import os
import subprocess
from datetime import datetime
import ROOT
from tabulate import tabulate
import pandas as pd
import argparse

def format_wp(wp):
    formatted_wp = f"m{abs(wp)}" if wp < 0 else f"{wp}"
    formatted_wp = formatted_wp.replace(".", "p")
    return formatted_wp

def calculate_weighted_yields(df, signal_regions, processes):
    print("--> Calculating weighted yields")
    weighted_yields = pd.DataFrame(index=processes.keys(), columns=signal_regions.keys())

    for region, selection in signal_regions.items():
        print(f"----> Processing region: {region}")
        df_region = df.Filter(selection)

        for process, samples in processes.items():
            print(f"------> Processing process: {process}")
            sample_filter = " || ".join([f"sample==\"{sample}\"" for sample in samples])
            df_process = df_region.Filter(sample_filter)

            total_weight = df_process.Sum("weight").GetValue()
            weighted_yields.at[process, region] = total_weight

        weighted_yields.at["Background", region] = sum(weighted_yields.loc[process, region] for process in processes if process not in ["ggF", "ggFKL10", "VBFSM"])
    return weighted_yields


def create_and_compile_latex_table(df, caption, output_file, output_directory):
    print(f"---> Creating and compiling LaTeX table: {output_file}")
    with open(output_file, "w") as f:
        f.write(r"\documentclass{article}" + "\n")
        f.write(r"\usepackage{booktabs}" + "\n")
        f.write(r"\usepackage[utf8]{inputenc}" + "\n")
        f.write(r"\usepackage{caption}" + "\n")
        f.write(r"\begin{document}" + "\n")

        f.write(r"\begin{table}[ht]" + "\n")
        f.write(r"\centering" + "\n")
        f.write(df.to_latex(float_format="{:.3f}".format))
        f.write(r"\caption{" + caption + "}" + "\n")
        f.write(r"\end{table}" + "\n")

        f.write(r"\end{document}" + "\n")

    subprocess.call(["pdflatex", "-output-directory", output_directory, output_file])
    # Remove intermediate files
    os.remove(output_file.replace(".tex", ".aux"))
    os.remove(output_file.replace(".tex", ".log"))

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--scan', action='store_true', help='Perform WP scan from -1 to 1 with a distance of 0.1')
    args = parser.parse_args()

    print("--> Initializing script")
    InPut = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/EB_Reader_dev/tree-ALL.root"
    outDir = f"/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool_outdir/tables_EB_V01_tree_{datetime.now().strftime('%Y%m%d')}"
    os.makedirs(outDir, exist_ok=True)

    PreSelection = "OS==1&&n_btag==2&&mBB<150000"
    BDT = "SMBDT_ggF_VBF"
    WP = -0.13

    ROOT.gInterpreter.Declare("""
    int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
      int m_region = -1;
      if(!hasVBFJetCandidates){
        m_region = 0;
      }else{
        if(SMBDTggFVBF > cut ){
          m_region = 0;
        }else{
          m_region = 1;
        }
      }
      return m_region;
    }
    """)

    processes = {
        "ggF": ['hhttbb'],
        "ggFKL10": ['hhttbbL10'],
        "VBFSM": ['hhttbbVBFSM'],
        "Diboson": ["WZ", "ZZ", "WW"],
        "Zttjets": ["Zttbb", "Zttbl", "Zttbc", "Zttl", "Zttcc", "Zttcl"],
        "Zjets": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc"],
        "Stop": ["stops", "stopt", "stopWt"],
        "ttV": ["ttW", "ttZ"],
        "SHiggs": ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau"],
        "ttbar": ["ttbar"],
        "Others": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc", "Zl", "W", "W", "Wtt", "DY", "DYtt"],
        "Fakes": ["WFake", "WttFake", "ttbarFake"],
    }

    signal_regions = {
        "low-mHH-GGFSR": f"mHH<350000&&Category(hasVBFJetCandidates,{BDT},{str(WP)})==0",
        "high-mHH-GGFSR": f"mHH>350000&&Category(hasVBFJetCandidates,{BDT},{str(WP)})==0",
        "VBFSR": f"Category(hasVBFJetCandidates,{BDT},{str(WP)})==1",
    }

    df = ROOT.RDataFrame("Nominal", InPut)
    df = df.Filter(PreSelection)

    if args.scan:
        wp_range = list(map(lambda x: round(x, 2), [i * 0.01 for i in range(-100, 101)]))
    else:
        wp_range = [WP]

    for wp in wp_range:
        print(f"--> Processing WP: {wp}")
        formatted_wp = format_wp(wp)
        signal_regions = {
            "low-mHH-GGFSR": f"mHH<350000&&Category(hasVBFJetCandidates,{BDT},{str(wp)})==0",
            "high-mHH-GGFSR": f"mHH>350000&&Category(hasVBFJetCandidates,{BDT},{str(wp)})==0",
            "VBFSR": f"Category(hasVBFJetCandidates,{BDT},{str(wp)})==1",
        }

        weighted_yields = calculate_weighted_yields(df, signal_regions, processes)
        weighted_yields["Total"] = weighted_yields.sum(axis=1)

        # Creating the first table
        print("--> Creating first table")
        caption_1 = f"Weighted yields for each process and signal region (WP: {wp})"
        output_file_1 = os.path.join(outDir, f"yield_table_1_WP{formatted_wp}.tex")
        create_and_compile_latex_table(weighted_yields, caption_1, output_file_1, outDir)

        # Creating the second table
        print("--> Creating second table")
        processes_table_2 = {
            "ggF": processes["ggF"],
            "VBFSM": processes["VBFSM"],
            "Background": [],
        }
        weighted_yields_table_2 = weighted_yields.loc[list(processes_table_2.keys()), :]
        caption_2 = f"Weighted yields for ggF, VBFSM, and backgrounds in each signal region (WP: {wp})"
        output_file_2 = os.path.join(outDir, f"yield_table_2_WP{formatted_wp}.tex")
        create_and_compile_latex_table(weighted_yields_table_2, caption_2, output_file_2, outDir)

if __name__ == "__main__":
    main() 