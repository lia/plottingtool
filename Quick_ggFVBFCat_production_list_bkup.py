import os
import argparse
import sys
import ROOT
from decimal import Decimal
from tqdm import tqdm
import numpy as np

BDT='SMggFVBFBDTScore'
#processes to save
#Processes = ["hhttbb"]
Processes = ["Fake","data","ggFHtautau","hhttbb","DY","stopWt","stops","stopt","ttbar","ttbarFake","ttH","ttW","ttZ","hhttbbVBFl1cvv1cv1p5","hhttbbVBFl1cvv0p5cv1","hhttbbVBFl0cvv0cv1","hhttbbVBFl10cvv1cv1","hhttbbVBFl0cvv1cv1","hhttbbVBFl1cvv1cv0p5","hhttbbVBFl1cvv2cv1","hhttbbVBFl2cvv1cv1","hhttbbVBFl1cvv0cv1","hhttbbVBFl1cvv1p5cv1","hhttbbVBFSM","hhttbbVBFl1cvv3cv1","VBFHtautau","ggZHtautau","qqZHtautau","WHtautau","ggZHbb","qqZHbb","WHbb","qZHbb","W","WFake","WZ","WW","Wtt","WttFake","Zbc","Zbb","Zbl","Zcc","Zcl","Zl","ZZ","Zttbb","Zttbc","Zttbl","Zttl","Zttcl","Zttcc","DYtt"]
KRW_Processes = ['hhttbbKLRW0p0',"hhttbbKLRW20p0"]
Hist = {}
BDT_Names = ['SMVBFBDTScore','SM_NNscore']
#BDT_Names = ['SMVBFBDTScore']
# Select events for the analysis
ROOT.gInterpreter.Declare("""
  int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
    int m_region = -1;
    if(!hasVBFJetCandidates){
      m_region = 0;
    }else{
      if(SMBDTggFVBF > cut ){
        m_region = 0;
      }else{
        m_region = 1;
      }
    }
    return m_region;
  }
  """)


########################
#Quick Projection:
########################
def translator(in_name):
  out_name = in_name.replace("hhttbbKLRW","weight_klrw_")
  return out_name


def run(inFile,inFile_KRW,outFile,min_score,max_score,nbins,version):
  inner_run(inFile,min_score,max_score,nbins,version,Processes)
  inner_run(inFile_KRW,min_score,max_score,nbins,version,KRW_Processes)
  save(outFile)
def inner_run(inFile,min_score,max_score,nbins,version,processes):
  score_range = (min_score,max_score)
  #Hist = {}
  print(f"-->Opening {inFile}")
  df = ROOT.RDataFrame("Nominal", inFile)
  print(f"---->Applying cut n_btag==2")
  df = df.Filter("n_btag==2")
  print(f"---->Applying cut OS==1")
  df = df.Filter("OS==1")
  print(f"---->Applying cut mBB < 150000")
  df = df.Filter('mBB < 150000.')
  suffix = "_".join(["2tag","0mHH","OS"])
  for cur_proc in processes:
    print(f'-->Working on {cur_proc}')
    #df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    if "KLRW" in cur_proc:
      Weight = translator(cur_proc)
      print(Weight)
      df_tmp = df.Filter(f'(sample == "hhttbb")')
    else:
      Weight = "weight"
      df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    for cur_BDT in BDT_Names:
      wid = (score_range[1]-score_range[0])/nbins
      for i in tqdm(range(nbins)):
      #for i in range(nbins):
        xi = np.round(score_range[0]+i*wid,decimals=2)
        #print(xi)
        df_ggF_like = df_tmp.Filter(f"Category(hasVBFJetCandidates,{BDT},{xi}) == 0")
        name = "_".join([cur_proc,suffix,"GGFSR",cur_BDT,str(np.round(xi,decimals=2)).replace("-","m").replace(".","p")])
        h = (df_ggF_like.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{cur_BDT}", Weight))
        Hist[name] = h.GetValue().Clone()
        df_VBF_like = df_tmp.Filter(f"Category(hasVBFJetCandidates,{BDT},{xi}) == 1")
        name = "_".join([cur_proc,suffix,"VBFSR",cur_BDT,str(np.round(xi,decimals=2)).replace("-","m").replace(".","p")])
        h = (df_VBF_like.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{cur_BDT}", Weight))
        Hist[name] = h.GetValue().Clone()
  #fout = ROOT.TFile.Open(outFile, "RECREATE")

def save(out):
  fout = ROOT.TFile.Open(out, "RECREATE")
  print(f"-->Saving {out}")
  for cur_hist in Hist:
    Hist[cur_hist].Write()

  fout.Close()

if __name__ == "__main__" :
  parser = argparse.ArgumentParser()
  parser.add_argument("--input", action = "store", dest = "inFile", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V18.root')
  parser.add_argument("--input_KRW", action = "store", dest = "inFile_KRW", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V18_hh_bbtt.root')
  parser.add_argument("--output", action = "store", dest = "outFile", default = "./hist-mc16ade-SM-SLT-ALL_APC_V18_test.root")
  parser.add_argument("--min_score", action = "store", dest = "min_score", default = "-1",type=float)
  parser.add_argument("--max_score", action = "store", dest = "max_score", default = "1",type=float)
  parser.add_argument("--number_bins", action = "store", dest = "nbins", default = "20",type=int)
  parser.add_argument("--version", action = "store", dest = "version", default = "scan")
  args = vars(parser.parse_args())
  run(**args)


