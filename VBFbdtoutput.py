import ROOT as r
from array import array

def binHist(h):
    h.SetBinContent(1, h.GetBinContent(0)+h.GetBinContent(1))
    h.SetBinContent(h.GetNbinsX(), h.GetBinContent(h.GetNbinsX())+h.GetBinContent(h.GetNbinsX()+1))

f = r.TFile("VBFvsGGFoutput.root", "read")
f1 = r.TFile("GGFTree.root", "read")
f2 = r.TFile("VBFTree.root", "read")

h = r.TH1D("h", "", 60, 0., 500.)
h.Sumw2()
h1 = r.TH1D("h1", "", 60, 0., 500.)
h1.Sumw2()
h2 = r.TH1D("h2", "", 60, 0., 500.)
h2.Sumw2()
h3 = r.TH1D("h3", "", 60, 0., 500.)
h3.Sumw2()

GGFbdt = f.Get("dataset/Method_BDT/BDT/MVA_BDT_S")
VBFbdt = f.Get("dataset/Method_BDT/BDT/MVA_BDT_B")

t1 = f1.Get("Nominal")
t2 = f2.Get("Nominal")

GGFbdt.SetDirectory(0)
VBFbdt.SetDirectory(0)

c = r.TCanvas()

for entryNum in range(0, t1.GetEntries()):
    t1.GetEntry(entryNum)

    if t1.same_sign:
        continue

    if t1.n_btag != 2:
        continue
   
    if t1.nfjets < 2:
        continue

    GGFmHH = t1.mHH
    h.Fill(GGFmHH, t1.EventWeight)
    

for entryNum in range(0, t2.GetEntries()):
    t2.GetEntry(entryNum)

    if t2.same_sign:
        continue

    if t2.n_btag != 2:
        continue

    if t2.nfjets < 2:
        continue

    VBFmHH = t2.mHH
    h1.Fill(VBFmHH, t2.EventWeight)

for entryNum in range(0, t1.GetEntries()):
    t1.GetEntry(entryNum)

    if t1.same_sign:
        continue
        
    if t1.n_btag != 2:
        continue

    if t1.nfjets >= 2:
        continue

    GGFmJJ = t1.mJJ
    h2.Fill(GGFmJJ, t1.EventWeight)

for entryNum in range(0, t2.GetEntries()):
    t2.GetEntry(entryNum)

    if t2.same_sign:
        continue

    if t2.n_btag != 2:
        continue

    if t2.nfjets >= 2:
        continue

    VBFmJJ = t2.mJJ
    h3.Fill(VBFmJJ, t2.EventWeight)

#After requiring that nfjets < 2
nGGF_Lessfjet = h.Integral(0, -1)
nVBF_Lessfjet = h1.Integral(0, -1)

#After requiring that nfjets >= 2
nGGF_Morefjet = h2.Integral(0, -1) 
nVBF_Morefjet = h3.Integral(0, -1)

GGFbdt.SetStats(0)
VBFbdt.SetStats(0)

GGFbdt.Scale(1./GGFbdt.Integral())
VBFbdt.Scale(1./VBFbdt.Integral())
    
GGFbdt.SetTitle("BDT Output of GGF and VBF")
GGFbdt.SetLineWidth(3)
GGFbdt.SetLineColor(r.kPink-9)
VBFbdt.SetLineWidth(3)
VBFbdt.SetLineColor(r.kViolet+1)
    
GGFbdt.Draw("hist")
VBFbdt.Draw("hist same")

maxi = GGFbdt.GetMaximum()
if VBFbdt.GetMaximum() > maxi:
    maxi = VBFbdt.GetMaximum()

GGFbdt.GetXaxis().SetRangeUser(-1, 1)
GGFbdt.GetYaxis().SetRangeUser(0, maxi*1.1)
GGFbdt.GetXaxis().SetTitle("BDT Response")
GGFbdt.GetYaxis().SetTitle("(1/N) dN/dx")
        
legend = r.TLegend(0.25, 0.55, 0.5, 0.63)
legend.AddEntry(GGFbdt, "GGF", "F")
legend.AddEntry(VBFbdt, "VBF", "F")
legend.SetLineWidth(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.04)
latex.DrawLatex(0.25, 0.79, "#it{ATLAS} #bf{Internal}")
latex.SetTextSize(0.034)
latex.DrawText(0.25, 0.75, "Number of Events:")
latex.SetTextSize(0.03)
latex.DrawText(0.25, 0.71, "n of GGF Events = {:.3f}".format(nGGF_Lessfjet))
latex.SetTextSize(0.03)
latex.DrawText(0.25, 0.67, "n of VBF Events = {:.3f}".format(nVBF_Lessfjet))

c.Draw()
    
c.SaveAs("VBFbdtoutput.pdf")



##########
## Cut efficiency
##########

# GGFbdt
# VBFbdt

def getIntegrals(h, binNr):
    if binNr == h.GetNbinsX()+1:
        upperInt = 0
    else:
        upperInt = h.Integral(binNr, h.GetNbinsX()+1)
    if binNr < 1:
        lowerInt = 0
    else:
        lowerInt = h.Integral(0, binNr-1)
    return lowerInt, upperInt

def scanHistogram(h):
    a_lowerInts = []
    a_upperInts = []
    a_x = []
    for i in range(0, h.GetNbinsX()+2):
        tmp_lowerInt, tmp_upperInt = getIntegrals(h, i)
        a_lowerInts.append(tmp_lowerInt*(nVBF_Morefjet/(nGGF_Morefjet+nVBF_Morefjet)))
        a_upperInts.append(tmp_upperInt*(nVBF_Lessfjet/(nGGF_Lessfjet+nVBF_Lessfjet)))
        a_x.append(h.GetXaxis().GetBinCenter(i) - h.GetXaxis().GetBinWidth(i)/2.0)
    return a_x, a_lowerInts, a_upperInts


# scale the histograms to reflect the actual #events
GGFbdt.Scale(1./nGGF_Lessfjet+nVBF_Lessfjet)
VBFbdt.Scale(1./nGGF_Lessfjet+nVBF_Lessfjet)

ggf_cuts, ggf_lowerInts, ggf_upperInts = scanHistogram(GGFbdt)
vbf_cuts, vbf_lowerInts, vbf_upperInts = scanHistogram(VBFbdt)

tg_ggf_lowerInts = r.TGraph(len(ggf_cuts), array('d', ggf_cuts), array('d', ggf_lowerInts))
tg_ggf_upperInts = r.TGraph(len(ggf_cuts), array('d', ggf_cuts), array('d', ggf_upperInts))

tg_vbf_lowerInts = r.TGraph(len(vbf_cuts), array('d', vbf_cuts), array('d', vbf_lowerInts))
tg_vbf_upperInts = r.TGraph(len(vbf_cuts), array('d', vbf_cuts), array('d', vbf_upperInts))

tg_ggf_lowerInts.SetLineColor(r.kPink-9)
tg_ggf_lowerInts.SetLineWidth(3)

tg_ggf_upperInts.SetLineColor(r.kPink-9)
tg_ggf_upperInts.SetLineWidth(3)

tg_vbf_lowerInts.SetLineColor(r.kViolet+1)
tg_vbf_lowerInts.SetLineWidth(3)

tg_vbf_upperInts.SetLineColor(r.kViolet+1)
tg_vbf_upperInts.SetLineWidth(3)

#### VBF Category ####
clower = r.TCanvas()

tg_ggf_lowerInts.SetTitle("VBF Category")
tg_ggf_lowerInts.GetXaxis().SetTitle("BDT cuts")
tg_ggf_lowerInts.GetYaxis().SetTitleSize(0.0285)
tg_ggf_lowerInts.GetYaxis().SetTitle("Fraction of Events w.r.t total number of ggF + VBF events in n_fjets < 2") 

tg_ggf_lowerInts.Draw()
tg_vbf_lowerInts.Draw("same")

legend = r.TLegend(0.15, 0.45, 0.4, 0.53)
legend.AddEntry(tg_ggf_lowerInts, "ggF Events")
legend.AddEntry(tg_vbf_lowerInts, "VBF Events")
legend.SetLineWidth(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.04)
latex.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
latex.SetTextSize(0.034)
latex.DrawLatex(0.15, 0.76, "#splitline{Number of VBF and GGF Events as a function}{of BDT cuts in the VBF Category}")
latex.SetTextSize(0.033)
latex.DrawLatex(0.15, 0.70, "Ratio of VBF Events to ggF Events:")
latex.SetTextSize(0.03)
latex.DrawText(0.15, 0.67, "After requiring n_fjets >= 2: VBF/ggF = {:.3f}".format(nVBF_Morefjet/nGGF_Morefjet))
latex.SetTextSize(0.03)
latex.DrawText(0.15, 0.64, "After requiring n_fjets < 2: VBF/ggF = {:.3f}".format(nVBF_Lessfjet/nGGF_Lessfjet))

clower.SaveAs("C_VBFCategory_Yields.pdf")

#### ggF Category ####
cupper = r.TCanvas()

tg_ggf_upperInts.SetTitle("ggF Category")
tg_ggf_upperInts.GetXaxis().SetTitle("BDT cuts")
tg_ggf_upperInts.GetYaxis().SetTitleSize(0.0285)
tg_ggf_upperInts.GetYaxis().SetTitle("Fraction of Events w.r.t total number of ggF + VBF events in n_fjets >= 2")

tg_ggf_upperInts.Draw()
tg_vbf_upperInts.Draw("same")

legend = r.TLegend(0.55, 0.75, 0.8, 0.83)
legend.AddEntry(tg_ggf_upperInts, "ggF Events")
legend.AddEntry(tg_vbf_upperInts, "VBF Events")
legend.SetLineWidth(0)
legend.Draw("same")

latex = r.TLatex()
latex.SetNDC()
latex.SetTextSize(0.04)
latex.DrawLatex(0.15, 0.60, "#it{ATLAS} #bf{Internal}")
latex.SetTextSize(0.034)
latex.DrawLatex(0.15, 0.54, "#splitline{Number of VBF and GGF Events as}{a function of BDT cuts in the ggF Category}")
latex.SetTextSize(0.033)
latex.DrawLatex(0.15, 0.48, "Ratio of VBF Events to ggF Events:")
latex.SetTextSize(0.03)
latex.DrawText(0.15, 0.45, "After requiring n_fjets >= 2: VBF/ggF = {:.3f}".format(nVBF_Morefjet/nGGF_Morefjet))
latex.SetTextSize(0.03)
latex.DrawText(0.15, 0.42, "After requiring n_fjets < 2: VBF/ggF = {:.3f}".format(nVBF_Lessfjet/nGGF_Lessfjet))

cupper.SaveAs("C_GGFCategory_Yields.pdf")

