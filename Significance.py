import os
import sys
import ROOT
from tqdm import tqdm
import numpy as np
import pandas as pd
from scipy.interpolate import make_interp_spline
#from PyQt5 import QtWidgets
#QtWidgets.QApplication(sys.argv)
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import rc
#import atlas_mpl_style as ampl
from atlasify import atlasify
from atlasify import monkeypatch_axis_labels
monkeypatch_axis_labels()
#import atlasplots as aplt
#from helper import ofufbins, create_default_legend, calculate_separation_index
#rc('font',**{'family':'serif','serif':['Roman']})
#rc('text', usetex=True)
from matplotlib import rcParams
rcParams["pdf.fonttype"] = 42
rcParams["ps.fonttype"] = 42

pwd = os.getcwd()
path_plots = os.path.join(pwd,'plots_Reader_EBR_V02_ggF_VBF_V03')
#path_plots = os.path.join(pwd,'plots2')
os.makedirs(path_plots, exist_ok=True)
#path = '/afs/cern.ch/work/l/lia/public/bbtautau/K2VOptimization/Training/tree-mc16ade-SM-SLT-ALL_APC_V13.root'
path = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/Reader_EBR_V02/tree-extsh2211.root"
#path = '/afs/cern.ch/work/l/lia/public/bbtautau/K2VOptimization/Training/test.root'
histos = {}
Yields = {}
histosVBFSR = {}
YieldsVBFSR = {}
K2V=['hhttbbVBFSM','hhttbbVBFl1cvv0cv1','hhttbbVBFl1cvv0p5cv1','hhttbbVBFl1cvv1p5cv1','hhttbbVBFl1cvv2cv1','hhttbbVBFl1cvv3cv1']
ggF=['hhttbb']
VBF=['hhttbbVBFSM']
BDT='SMggFVBFBDTScore_V03'
#Bkgs = ["ttH","ttW","ttZ", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau", "Wtt", "W", "Zbl", "Zbb", "Zbc", "Zcl", "Zcc", "Zl", "Zttbl", "Zttbb", "Zttbc", "Zttcl", "Zttcc", "WW", "WZ", "ZZ", "ttbar", "stops", "stopt", "stopWt", "ttbarFake", "WFake", "WttFake", "DY", "DYtt"]
Bkgs = ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau","W", "WZ", "WW", "Wtt", "Zcc", "Zbl", "Zcl", "Zbb", "Zbc", "Zl", "ZZ", "DY", "DYtt", "Zttbl", "Zttcl", "Zttl", "Zttbb", "Zttbc","Zttcc", "WFake", "WttFake", "ttbarFake", "ttbar", "stops", "stopt", "stopWt", "ttW", "ttZ"],
# Select events for the analysis
ROOT.gInterpreter.Declare("""
  int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
    int m_region = -1;
    if(!hasVBFJetCandidates){
      m_region = 0;
    }else{
      if(SMBDTggFVBF > cut ){
        m_region = 0;
      }else{
        m_region = 1;
      }
    }
    return m_region;
  }
  """)
def NonNeg(a):
  return a if a >= 0 else 0

def Z0(S, B):
  if S+B == 0:
    return 0
  else:
    return S/ROOT.TMath.Sqrt(S+B)


def Significance(df_s,df_b, func=Z0, score_range=(-1, 1), nbins=200):
  df_s = df_s.Filter("hasVBFJetCandidates == 1")
  df_b = df_b.Filter("hasVBFJetCandidates == 1")
  S0 = df_s.Sum('weight').GetValue()
  B0 = df_b.Sum('weight').GetValue()
  print('initial: S0={:.2f}, B0={:.2f}'.format(S0, B0))
  print('inclusive Z: {:.2f}'.format(func(S0, B0)))

  wid = (score_range[1]-score_range[0])/nbins
  arr_x = np.round(np.array([score_range[0]+i*wid for i in range(nbins)]), decimals=2)
  arr_Z = np.zeros([nbins])

  for i in tqdm(range(nbins)):
    xi = score_range[0]+i*wid
    df_s_tmp = df_s.Filter('{} >= {}'.format(BDT,str(xi)))
    Si = df_s_tmp.Sum('weight').GetValue()
    df_b_tmp = df_b.Filter('{} >= {}'.format(BDT,str(xi)))
    Bi = df_b_tmp.Sum('weight').GetValue()
    Zi = func(Si, Bi)
    if Zi<0: Zi=0
    #Zi = np.sqrt(i)
    arr_Z[i]=Zi
                  
    df_Z_pd = pd.DataFrame(data=arr_Z, index=arr_x, columns=["Z"])         
  return df_Z_pd

def Significance2(df_s,df_b, func=Z0, score_range=(-1, 1), nbins=200):

  wid = (score_range[1]-score_range[0])/nbins
  arr_x = np.round(np.array([score_range[0]+i*wid for i in range(nbins)]), decimals=2)
  arr_Z_ggF_like = np.zeros([nbins])
  arr_Z_VBF_like = np.zeros([nbins])
  for i in tqdm(range(nbins)):
    xi = score_range[0]+i*wid
    df_s_ggF_like = df_s.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0")
    df_b_ggF_like = df_b.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0")
    df_s_VBF_like = df_s.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
    df_b_VBF_like = df_b.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
    
    Ni_s_ggF_like = df_s_ggF_like.Sum('weight').GetValue()
    Ni_b_ggF_like = df_b_ggF_like.Sum('weight').GetValue()
    Ni_s_VBF_like = df_s_VBF_like.Sum('weight').GetValue()
    Ni_b_VBF_like = df_b_VBF_like.Sum('weight').GetValue()
    
    Zi_ggF_like = NonNeg(func(Ni_s_ggF_like, Ni_b_ggF_like))
    Zi_VBF_like = NonNeg(func(Ni_b_VBF_like, Ni_s_VBF_like))
    
    arr_Z_ggF_like[i]=Zi_ggF_like
    arr_Z_VBF_like[i]=Zi_VBF_like
                  
    df_Z_pd = pd.DataFrame({"Z_ggF_like": arr_Z_ggF_like,
                            "Z_VBF_like": arr_Z_VBF_like
                            }, index=arr_x)         
  return df_Z_pd


#def Plot_Significance(df_s, df_b):
#  
#  return

def Efficiency(df_s,df_b,score_range=(-1, 1), nbins=100):
  S0 = df_s.Sum('weight').GetValue()
  B0 = df_b.Sum('weight').GetValue()
  df_s_VBFJets = df_s.Filter("hasVBFJetCandidates==1")
  df_b_VBFJets = df_b.Filter("hasVBFJetCandidates==1")
  S_VBFJets = df_s_VBFJets.Sum('weight').GetValue()
  B_VBFJets = df_b_VBFJets.Sum('weight').GetValue()
  wid = (score_range[1]-score_range[0])/nbins
  arr_x = np.round(np.array([score_range[0]+i*wid for i in range(nbins)]), decimals=2)
  
  arr_ES_ggF_0_ggF_like   = np.zeros([nbins])
  arr_EB_VBF_0_ggF_like   = np.zeros([nbins])
  arr_ES_ggF_VBFJets_ggF_like = np.zeros([nbins])
  arr_EB_VBF_VBFJets_ggF_like = np.zeros([nbins])
 
  arr_ES_ggF_0_VBF_like = np.zeros([nbins])
  arr_EB_VBF_0_VBF_like = np.zeros([nbins])
  arr_ES_ggF_VBFJets_VBF_like = np.zeros([nbins])
  arr_EB_VBF_VBFJets_VBF_like = np.zeros([nbins])

  arr_ES = np.zeros([nbins])
  arr_EB = np.zeros([nbins])
  for i in tqdm(range(nbins)):
    xi = score_range[0]+i*wid
    df_s_ggF_like = df_s.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0") 
    df_s_VBF_like = df_s.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
    df_b_ggF_like = df_b.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0") 
    df_b_VBF_like = df_b.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
    
    df_s_VBFJets_ggF_like = df_s_VBFJets.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0") 
    df_s_VBFJets_VBF_like = df_s_VBFJets.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
    df_b_VBFJets_ggF_like = df_b_VBFJets.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 0") 
    df_b_VBFJets_VBF_like = df_b_VBFJets.Filter(f"Category(hasVBFJetCandidates,{BDT},{str(xi)}) == 1")
   
    Ni_s_ggF_like = df_s_ggF_like.Sum('weight').GetValue()
    Ni_s_VBF_like = df_s_VBF_like.Sum('weight').GetValue()
    Ni_b_ggF_like = df_b_ggF_like.Sum('weight').GetValue()
    Ni_b_VBF_like = df_b_VBF_like.Sum('weight').GetValue()

    Ni_s_VBFJets_ggF_like = df_s_VBFJets_ggF_like.Sum('weight').GetValue()
    Ni_s_VBFJets_VBF_like = df_s_VBFJets_VBF_like.Sum('weight').GetValue()
    Ni_b_VBFJets_ggF_like = df_b_VBFJets_ggF_like.Sum('weight').GetValue()
    Ni_b_VBFJets_VBF_like = df_b_VBFJets_VBF_like.Sum('weight').GetValue()
    
    Effi_s_ggF_like = (Ni_s_ggF_like / S0 if S0!=0 else 0.)
    Effi_s_VBF_like = (Ni_s_VBF_like / S0 if S0!=0 else 0.)
    Effi_b_ggF_like = (Ni_b_ggF_like / B0 if B0!=0 else 0.)
    Effi_b_VBF_like = (Ni_b_VBF_like / B0 if B0!=0 else 0.)

    Effi_s_VBFJets_ggF_like = (Ni_s_VBFJets_ggF_like / S_VBFJets if S_VBFJets!=0 else 0.)
    Effi_s_VBFJets_VBF_like = (Ni_s_VBFJets_VBF_like / S_VBFJets if S_VBFJets!=0 else 0.)
    Effi_b_VBFJets_ggF_like = (Ni_b_VBFJets_ggF_like / B_VBFJets if B_VBFJets!=0 else 0.)
    Effi_b_VBFJets_VBF_like = (Ni_b_VBFJets_VBF_like / B_VBFJets if B_VBFJets!=0 else 0.)

    arr_ES_ggF_0_ggF_like[i] = Effi_s_ggF_like 
    arr_ES_ggF_0_VBF_like[i] = Effi_s_VBF_like
    arr_EB_VBF_0_ggF_like[i] = Effi_b_ggF_like
    arr_EB_VBF_0_VBF_like[i] = Effi_b_VBF_like
    
    arr_ES_ggF_VBFJets_ggF_like[i] = Effi_s_VBFJets_ggF_like  
    arr_ES_ggF_VBFJets_VBF_like[i] = Effi_s_VBFJets_VBF_like 
    arr_EB_VBF_VBFJets_ggF_like[i] = Effi_b_VBFJets_ggF_like 
    arr_EB_VBF_VBFJets_VBF_like[i] = Effi_b_VBFJets_VBF_like 
    
    df = pd.DataFrame({'ES_ggF_0_ggF_like'       : arr_ES_ggF_0_ggF_like,
                       'ES_ggF_0_VBF_like'       : arr_ES_ggF_0_VBF_like,
                       'EB_VBF_0_ggF_like'       : arr_EB_VBF_0_ggF_like,
                       'EB_VBF_0_VBF_like'       : arr_EB_VBF_0_VBF_like,
                       'ES_ggF_VBFJets_ggF_like' : arr_ES_ggF_VBFJets_ggF_like,
                       'ES_ggF_VBFJets_VBF_like' : arr_ES_ggF_VBFJets_VBF_like,
                       'EB_VBF_VBFJets_ggF_like' : arr_EB_VBF_VBFJets_ggF_like,
                       'EB_VBF_VBFJets_VBF_like' : arr_EB_VBF_VBFJets_VBF_like
                      }, index = arr_x)
  return df

def prepare_lineshape(x,y):
  X_Y_Spline = make_interp_spline(x, y)
  X_ = np.linspace(x.min(), x.max(), 500)
  Y_ = X_Y_Spline(X_)
  return X_,Y_
########################
#selections:
########################
df = ROOT.RDataFrame("Nominal", path)
df = df.Filter("n_btag == 2")
df = df.Filter('OS==1')
df = df.Filter('mBB < 150000.')
df = df.Filter('!sample.empty()')
df = df.Filter('mHH>0.')
#df = df.Filter('region == 1')
df = df.Define('MHH','mHH/1000.')
#df = df.Filter('TRandom3 rand; double r = rand.Rndm(); return r < 0.1')
    
df_ggF = df.Filter('||'.join([f"(sample == \"{ggf}\")" for ggf in ggF]))
df_VBF = df.Filter('||'.join([f"(sample == \"{vbf}\")" for vbf in VBF]))
df_bkg = df.Filter('||'.join([f"(sample == \"{bkg}\")" for bkg in Bkgs]))
########################
#plot Significance scan:
########################

#ggF Cat. and VBF Cat. siginificance
df_Z = Significance2(df_ggF, df_VBF)
max_index_ggF_like=df_Z["Z_ggF_like"].idxmax()
max_index_VBF_like=df_Z["Z_VBF_like"].idxmax()
print('max-Z ggF_like: {:.2f}'.format(df_Z.loc[max_index_ggF_like,"Z_ggF_like"]), 'cut threshold: [', max_index_ggF_like, ']')
print('max-Z VBF_like: {:.2f}'.format(df_Z.loc[max_index_VBF_like,"Z_VBF_like"]), 'cut threshold: [', max_index_VBF_like, ']')
X_, Y_ggF_like_ = prepare_lineshape(df_Z.index,df_Z["Z_ggF_like"])
X_, Y_VBF_like_ = prepare_lineshape(df_Z.index,df_Z["Z_VBF_like"])
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_ggF_like_)
plt.plot(X_, Y_VBF_like_)
star_ggF = ax.scatter(x=max_index_ggF_like, y=df_Z.loc[max_index_ggF_like,"Z_ggF_like"], c='red', marker="*")
star_VBF = ax.scatter(x=max_index_VBF_like, y=df_Z.loc[max_index_VBF_like,"Z_VBF_like"], c='violet', marker="*")
plt.xlabel("BDT Score ")
plt.ylabel("Significance")
txt1 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
txt2 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0) 
txt3 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
txt4 = atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Significance scan\n",enlarge=1.3)
plt.legend([txt1, txt2, txt3, star_ggF, star_VBF ,txt4], 
            ('max-Z ggF_like: {:.2f} cut threshold: [{:.2f}]'.format(df_Z.loc[max_index_ggF_like,"Z_ggF_like"],max_index_ggF_like),
             'max-Z VBF_like: {:.2f} cut threshold: [{:.2f}]'.format(df_Z.loc[max_index_VBF_like,"Z_VBF_like"],max_index_VBF_like),
              r"$Z = S/\sqrt{(S+B)}$",
              "ggF Cat. Significance Maximum",
              "VBF Cat. Significance Maximum"
            )
          )
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Significance_Z0_ggF_VBF_Cat.pdf")) 


#compute the significance
df_Z = Significance(df_ggF, df_VBF)
max_index=df_Z["Z"].idxmax()
print('max-Z: {:.2f}'.format(df_Z.loc[max_index,"Z"]), 'cut threshold: [', max_index, ']')
X_, Y_ = prepare_lineshape(df_Z.index,df_Z["Z"])
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_)
star = ax.scatter(x=max_index, y=df_Z.loc[max_index,"Z"], c='r', marker="*")
plt.xlabel("BDT Score ")
plt.ylabel("Significance")
txt1 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
txt2 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0) 
txt3 = atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Significance scan\n",enlarge=1.3)
plt.legend([txt1, txt2,star,txt3], ('max-Z: {:.2f} cut threshold: [{:.2f}]'.format(df_Z.loc[max_index,"Z"],max_index), r"$Z = S/\sqrt{(S+B)}$","Significance Maximun"))
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Significance_Z0_ggF_VBF.pdf")) 

#ggF/All backgrounds significance
df_Z = Significance(df_ggF, df_bkg)
max_index=df_Z["Z"].idxmax()
print('max-Z: {:.2f}'.format(df_Z.loc[max_index,"Z"]), 'cut threshold: [', max_index, ']')
X_, Y_ = prepare_lineshape(df_Z.index,df_Z["Z"])
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_)
star = ax.scatter(x=max_index, y=df_Z.loc[max_index,"Z"], c='r', marker="*")
plt.xlabel("BDT Score ")
plt.ylabel("Significance")
txt1 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
txt2 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0) 
txt3 = atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/All backgrounds Significance scan\n",enlarge=1.3)
plt.legend([txt1, txt2,star,txt3], ('max-Z: {:.2f} cut threshold: [{:.2f}]'.format(df_Z.loc[max_index,"Z"],max_index), r"$Z = S/\sqrt{(S+B)}$","Significance Maximun"))
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Significance_Z0_ggF_all_bkgs.pdf")) 

#VBF/All backgrounds significance
df_Z = Significance(df_VBF, df_bkg)
max_index=df_Z["Z"].idxmax()
print('max-Z: {:.2f}'.format(df_Z.loc[max_index,"Z"]), 'cut threshold: [', max_index, ']')
X_, Y_ = prepare_lineshape(df_Z.index,df_Z["Z"])
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_)
star = ax.scatter(x=max_index, y=df_Z.loc[max_index,"Z"], c='r', marker="*")
plt.xlabel("BDT Score ")
plt.ylabel("Significance")
txt1 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
txt2 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0) 
txt3 = atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/All backgrounds Significance scan\n",enlarge=1.3)
plt.legend([txt1, txt2,star,txt3], ('max-Z: {:.2f} cut threshold: [{:.2f}]'.format(df_Z.loc[max_index,"Z"],max_index), r"$Z = S/\sqrt{(S+B)}$","Significance Maximun"))
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Significance_Z0_VBF_all_bkgs.pdf")) 

########################
#plot Efficiencies:
########################
#compute the Efficiency
df_Eff_pd = Efficiency(df_ggF, df_VBF)
x=df_Eff_pd.index
#preparing line shapes
for cur in ["ES_ggF_0_ggF_like","ES_ggF_0_VBF_like","EB_VBF_0_ggF_like","EB_VBF_0_VBF_like","ES_ggF_VBFJets_ggF_like","ES_ggF_VBFJets_VBF_like","EB_VBF_VBFJets_ggF_like","EB_VBF_VBFJets_VBF_like"]:
  print(f"{cur}:\n")
  print(df_Eff_pd[cur])
#print(df_Eff_pd["ES_ggF_0_ggF_like"])
X_, Y_ES_ggF_0_ggF_like = prepare_lineshape(x,df_Eff_pd["ES_ggF_0_ggF_like"])
X_, Y_ES_ggF_0_VBF_like = prepare_lineshape(x,df_Eff_pd["ES_ggF_0_VBF_like"])
X_, Y_EB_VBF_0_ggF_like = prepare_lineshape(x,df_Eff_pd["EB_VBF_0_ggF_like"])
X_, Y_EB_VBF_0_VBF_like = prepare_lineshape(x,df_Eff_pd["EB_VBF_0_VBF_like"])
X_, Y_ES_ggF_VBFJets_ggF_like = prepare_lineshape(x,df_Eff_pd["ES_ggF_VBFJets_ggF_like"])
X_, Y_ES_ggF_VBFJets_VBF_like = prepare_lineshape(x,df_Eff_pd["ES_ggF_VBFJets_VBF_like"])
X_, Y_EB_VBF_VBFJets_ggF_like = prepare_lineshape(x,df_Eff_pd["EB_VBF_VBFJets_ggF_like"])
X_, Y_EB_VBF_VBFJets_VBF_like = prepare_lineshape(x,df_Eff_pd["EB_VBF_VBFJets_VBF_like"])

'''
ggF VBF in ggF Cat. denominator: after hasVBFJetsCandidates==1
'''
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_ES_ggF_VBFJets_ggF_like, color="r", label="ggF in ggF Cat.")
plt.plot(X_, Y_EB_VBF_VBFJets_ggF_like, color="b", label="VBF in ggF Cat.")
plt.xlabel("BDT Score ")
plt.ylabel("Efficiency")
atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Efficiency\n",enlarge=1.3)
plt.legend()
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Efficiency_ggF_VBF_in_ggF_VBFJets.pdf")) 

'''
ggF VBF in ggF Cat. denominator: after hasVBFJetsCandidates==1
'''
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_ES_ggF_VBFJets_VBF_like, color="r", label="ggF in VBF Cat.")
plt.plot(X_, Y_EB_VBF_VBFJets_VBF_like, color="b", label="VBF in VBF Cat.")
plt.xlabel("BDT Score ")
plt.ylabel("Efficiency")
atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Efficiency\n",enlarge=1.3)
plt.legend()
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Efficiency_ggF_VBF_in_VBF_VBFJets.pdf")) 

'''
ggF VBF in ggF Cat. denominator: without hasVBFJetsCandidates==1
'''
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_ES_ggF_0_ggF_like, color="r", label="ggF in ggF Cat.")
plt.plot(X_, Y_EB_VBF_0_ggF_like, color="b", label="VBF in ggF Cat.")
plt.xlabel("BDT Score ")
plt.ylabel("Efficiency")
atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Efficiency\n",enlarge=1.3)
plt.legend()
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Efficiency_ggF_VBF_in_ggF_Total.pdf")) 

'''
ggF VBF in ggF Cat. denominator: without hasVBFJetsCandidates==1
'''
fig, ax = plt.subplots(figsize=(8,6))
plt.plot(X_, Y_ES_ggF_0_VBF_like, color="r", label="ggF in VBF Cat.")
plt.plot(X_, Y_EB_VBF_0_VBF_like, color="b", label="VBF in VBF Cat.")
plt.xlabel("BDT Score ")
plt.ylabel("Efficiency")
atlasify("Internal",r"$HH\rightarrow b\bar{b}\tau^{+}\tau^{-}$"+"\n"
             "ggF/VBF Efficiency\n",enlarge=1.3)
plt.legend()
fig.tight_layout()
fig.savefig(os.path.join(path_plots,"Efficiency_ggF_VBF_in_VBF_Total.pdf")) 

