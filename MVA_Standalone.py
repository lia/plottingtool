import ROOT
import os
import numpy as np
import atlasplots as aplt
from pathlib import Path

def apply_mva(rdf, h_dict):
  report = rdf.Report()
  count = rdf.Count()
  rdf = rdf.Filter("hasVBFJetCandidates == 1", "VBF region selection")
  rdf = rdf.Filter("OS == 1", "OS selection")
  rdf = rdf.Filter("mBB < 150000.", "low mBB selection") 
  rdf = rdf.Filter("n_btag == 2", "btag") 
  n_vars = 7
  n_fold = 3
  idx_str = ""
  for n in range(n_fold):
    xml_dir = os.path.join(storage,"xml", "all_in_one", f'SM_BDT_lephad_SMggF_SMVBF_V03_PCBT_fold_{n}.weights.xml')

    ROOT.gInterpreter.ProcessLine(f'''
        TMVA::Experimental::RReader model_{n} ("{xml_dir}");
        auto computeModel_{n} = TMVA::Experimental::Compute<{n_vars}, float>(model_{n});
    ''')
    var_names = ROOT.TMVA.Experimental.RReader(xml_dir).GetVariableNames()
    for var in var_names:
      rdf = rdf.Redefine(var, f'(float)({var})')

    rdf = rdf.Define(
      f"BDTG_{n}",
      eval(f'ROOT.computeModel_{n}'),
      eval(f'ROOT.model_{n}.GetVariableNames()'),
      )

    idx_str += f'( (event_number % {n_fold} == {n}) * BDTG_{n}.front() ) {"+" if n < n_fold - 1 else ""} '

  rdf = rdf.Define("BDTG", idx_str)

  h = {
    k: rdf.Filter(v).Histo1D((f"bdtg", ";BDTG;Yields", 20, -1.0, 1.0), "BDTG", "weight") for k, v in
    h_dict.items()
  }
  # h = rdf.Histo1D((f"bdtg", ";BDTG;Yields", 100, -1.0, 1.0), "BDTG", "weight")

  print(f"Total Entries: {count.GetValue()}")
  report.Print()

  return {k: v.GetPtr() for k, v in h.items()}

def make_new_point(h_vbf, k2v: float = 1.0):
  from reweight import calculate_coefficient
  k2v_dict = {
              'VBFSM': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 1, 1)),
              'VBFl0cvv0cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(0, 0, 1)),
              'VBFl10cvv1cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 10, 1)),
              'VBFl1cvv0p5cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(0.5, 1, 1)),
              'VBFl1cvv3cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(3, 1, 1)),
              'VBFl2cvv1cv1': calculate_coefficient(k2v=k2v, base_k2v_kl_kv=(1, 2, 1)),
              }

  str = "+".join([f"h_vbf['{k}'] * {k2v_dict[k]}" for k in h_vbf.keys()])
  # print(str)

  return eval(str)


def norm(h):
  return np.array([y for y in h])/ np.linalg.norm([y for y in h])

if __name__ == "__main__" :

  dir = os.getcwd()
  storage = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/" 
  inFolder = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/ntuple_VBF03/tree-PCBT-ggfvbfV03-extsh2211.root"
  outDir = "MVA_Standalone"
  Path(outDir).mkdir(parents=True, exist_ok=True)
  rdf_ggf = ROOT.RDataFrame("Nominal", os.path.join(storage,"ntuple_VBF03", 'GGF.root'))
  rdf_vbf = ROOT.RDataFrame("Nominal", os.path.join(storage,"ntuple_VBF03", 'VBF.root'))
  
  h_ggf = apply_mva(rdf_ggf, {'ggF': f'sample == \"hhttbb\"'})['ggF']
  h_vbf = apply_mva(rdf_vbf, {
    'VBFSM': f'sample == \"hhttbbVBFSM\"',
    'VBFl0cvv0cv1': f'sample == \"hhttbbVBFl0cvv0cv1\"',
    'VBFl10cvv1cv1': f'sample == \"hhttbbVBFl10cvv1cv1\"',
    'VBFl1cvv0p5cv1': f'sample == \"hhttbbVBFl1cvv0p5cv1\"',
    'VBFl1cvv3cv1': f'sample == \"hhttbbVBFl1cvv3cv1\"',
    'VBFl2cvv1cv1': f'sample == \"hhttbbVBFl2cvv1cv1\"',
  }) 
  
  #setup style
  ROOT.gROOT.SetBatch()
  aplt.set_atlas_style()
  
  fig, ax = aplt.subplots(1, 1)

  h_ggf.Scale(1./h_ggf.Integral())
  h_vbf["VBFSM"].Scale(1./h_vbf["VBFSM"].Integral()) 
  ax.plot(h_ggf, label="hhttbb", linecolor = ROOT.kRed,  labelfmt="L")
  ax.plot(h_vbf["VBFSM"], label="hhttbbVBFSM", linecolor = ROOT.kBlue,  labelfmt="L") 

  ax.add_margins(top=0.30)
  # Set axis titles
  ax.set_xlabel("SMggFVBFScore_V03")
  ax.set_ylabel("Normalised to unity")
  # Add the ATLAS Label
  aplt.atlas_label(text="Internal", loc="upper left")
  ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
  ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau_{lep}#tau_{had}", size=22, align=13)
  # Add legend
  #ax.legend(loc=(0.50, 0.50, 0.95, 0.92))
  ax.legend(loc=(0.45, 0.55, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))
  filename = os.path.join(outDir,"Standalone_BDT.pdf")
  print (f"-->Saving {filename}")
  fig.savefig(filename)
  fig.savefig(filename.replace(".pdf",".png"))
  fig.savefig(filename.replace(".pdf",".eps")) 


  






