import os
import argparse
import sys
import ROOT
from decimal import Decimal
from tqdm import tqdm
import numpy as np

BDT='SMggFVBFBDTScore_V03'
#processes to save
Processes = ["hhttbb"]
#Processes = ["Fake","data","ggFHtautau","hhttbb","DY","stopWt","stops","stopt","ttbar","ttbarFake","ttH","ttW","ttZ","hhttbbVBFl1cvv1cv1p5","hhttbbVBFl1cvv0p5cv1","hhttbbVBFl0cvv0cv1","hhttbbVBFl10cvv1cv1","hhttbbVBFl0cvv1cv1","hhttbbVBFl1cvv1cv0p5","hhttbbVBFl1cvv2cv1","hhttbbVBFl2cvv1cv1","hhttbbVBFl1cvv0cv1","hhttbbVBFl1cvv1p5cv1","hhttbbVBFSM","hhttbbVBFl1cvv3cv1","VBFHtautau","ggZHtautau","qqZHtautau","WHtautau","ggZHbb","qqZHbb","WHbb","qZHbb","W","WFake","WZ","WW","Wtt","WttFake","Zbc","Zbb","Zbl","Zcc","Zcl","Zl","ZZ","Zttbb","Zttbc","Zttbl","Zttl","Zttcl","Zttcc","DYtt"]
KRW_Processes = ['hhttbbKLRW0p0',"hhttbbKLRW20p0"]
cuts = ["true", "mHH>350000.", "mHH<350000."]
regions = ["GGFSR_0_350mHH", "GGFSR_350mHH", "VBFSR_0mHH"]
Hist = {}
#BDT_Names = ['SMVBFBDTScore','SM_NNscore','SMGGFBDTScore','SMGGFBDTScore_mHHsplit']
BDT_Names = ['SMVBFBDTScore']
# Select events for the analysis
ROOT.gInterpreter.Declare("""
  int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
    int m_region = -1;
    if(!hasVBFJetCandidates){
      m_region = 0;
    }else{
      if(SMBDTggFVBF > cut ){
        m_region = 0;
      }else{
        m_region = 1;
      }
    }
    return m_region;
  }
  """)


########################
#Quick Projection:
########################
def binning2090():
  BDTbin990 = [0]*1990
  BDTbin101 = [0]*101
  BDTbinning = [0]*2091

  size3 = 1990
  size4 = 101

  for j in range(size3):
    BDTbin990[j] = np.round(j/1000.0 - 1.0,decimals=4)
    BDTbinning[j] = BDTbin990[j]

  l = j + 1

  for j in range(size4):
    BDTbin101[j] = np.round(0.99 + j/10000.0,decimals=4)
    BDTbinning[l] = BDTbin101[j]
    l += 1
  return BDTbinning

def translator(in_name):
  out_name = in_name.replace("hhttbbKLRW","weight_klrw_")
  return out_name
def evaluate(score,df,proc,suffix,BDT,Weight):
  BDTbinning = binning2090()
  print(BDTbinning)
  exit(0)

  df_ggF_like = df.Filter(f"Category(hasVBFJetCandidates,{BDT},{score}) == 0")
  
  df_ggF_like_lowmHH = df_ggF_like.Filter("mHH<350000.")
  name = "_".join([proc,suffix,"GGFSR","0_350mHH",BDT,str(np.round(score,decimals=2)).replace("-","m").replace(".","p")])
  h = (df_ggF_like_lowmHH.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{BDT}", Weight))
  Hist[name] = h.GetValue().Clone()
  
  df_ggF_like_highmHH = df_ggF_like.Filter("mHH>350000.")
  name = "_".join([proc,suffix,"GGFSR","350mHH",BDT,str(np.round(score,decimals=2)).replace("-","m").replace(".","p")])
  h = (df_ggF_like_highmHH.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{BDT}", Weight))
  Hist[name] = h.GetValue().Clone()

  df_VBF_like = df.Filter(f"Category(hasVBFJetCandidates,{BDT},{score}) == 1")
  name = "_".join([proc,suffix,"VBFSR",BDT,str(np.round(score,decimals=2)).replace("-","m").replace(".","p")])
  h = (df_VBF_like.Histo1D(ROOT.RDF.TH1DModel(f"{name}", f"{name}", 1000, -1, 1), f"{BDT}", Weight))
  Hist[name] = h.GetValue().Clone()
  
def run(inFile,inFile_KRW,outFile,min_score,max_score,nbins,version,WP):
  inner_run(inFile,min_score,max_score,nbins,version,Processes,WP)
  inner_run(inFile_KRW,min_score,max_score,nbins,version,KRW_Processes,WP)
  save(outFile)
def inner_run(inFile,min_score,max_score,nbins,version,processes,WP):
  score_range = (min_score,max_score)
  #Hist = {}
  print(f"-->Opening {inFile}")
  df = ROOT.RDataFrame("Nominal", inFile)
  print(f"---->Applying cut n_btag==2")
  df = df.Filter("n_btag==2")
  print(f"---->Applying cut OS==1")
  df = df.Filter("OS==1")
  print(f"---->Applying cut mBB < 150000")
  df = df.Filter('mBB < 150000.')
  for cur_proc in processes:
    print(f'------>Working on {cur_proc}')
    #df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    if "KLRW" in cur_proc:
      Weight = translator(cur_proc)
      print(Weight)
      df_tmp = df.Filter(f'(sample == "hhttbb")')
    else:
      Weight = "weight"
      df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    for cur_cut in cuts:
      print(f'-------->Working on cut {cur_cut}')
      df_cut = df_tmp.Filter(f'{cur_cut}')  
      if cur_cut == "true":
        suffix = "_".join(["2tag","0mHH","OS"])
      elif (("mHH" in cur_cut) and ("<" in cur_cut) and ("350000" in cur_cut)):
        suffix = "_".join(["2tag","0_350mHH","OS"])
      elif (("mHH" in cur_cut) and (">" in cur_cut) and ("350000" in cur_cut)):
        suffix = "_".join(["2tag","350mHH","OS"])
      else:
        print(f"-------->Cut {cur_cut} is not supported")
        exit(1)
      for cur_BDT in BDT_Names:
        wid = (score_range[1]-score_range[0])/nbins
        if WP > -1 and WP < 1:
          print(f"---------->Evaluating WP = {np.round(WP,decimals=2)}")
          evaluate(np.round(WP,decimals=2),df_cut,cur_proc,suffix,cur_BDT,Weight)
        else:
          for i in tqdm(range(nbins)):
          #for i in range(nbins):
            xi = np.round(score_range[0]+i*wid,decimals=2)
            #print(xi)
            evaluate(xi,df_cut,cur_proc,suffix,cur_BDT,Weight)       

def save(out):
  fout = ROOT.TFile.Open(out, "RECREATE")
  print(f"-->Saving {out}")
  for cur_hist in Hist:
    Hist[cur_hist].Write()

  fout.Close()

if __name__ == "__main__" :
  parser = argparse.ArgumentParser()
  parser.add_argument("--input", action = "store", dest = "inFile", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V18.root')
  parser.add_argument("--input_KRW", action = "store", dest = "inFile_KRW", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/tree-mc16ade-SM-SLT-ALL_APC_V18_hh_bbtt.root')
  parser.add_argument("--output", action = "store", dest = "outFile", default = "./hist-mc16ade-SM-SLT-ALL_APC_V18_test.root")
  parser.add_argument("--min_score", action = "store", dest = "min_score", default = "-1",type=float)
  parser.add_argument("--max_score", action = "store", dest = "max_score", default = "1",type=float)
  parser.add_argument("--number_bins", action = "store", dest = "nbins", default = "20",type=int)
  parser.add_argument("--version", action = "store", dest = "version", default = "scan")
  parser.add_argument("--WP", action = "store", dest = "WP", default = "999", type=float)
  args = vars(parser.parse_args())
  run(**args)


