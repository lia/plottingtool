import os
import uproot
import ROOT
import atlasplots as aplt
#from helper import ofufbins, create_default_legend, calculate_separation_index

path = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/hist-mc16ade-SM-SLT-ALL_APC_V16.root'
outDir = '/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/plots'
subdir = 'Preselection_lowMbb150'
histos = {}
Yields = {}
histosVBFSR = {}
YieldsVBFSR = {}
K2Vs=['hhttbbVBFSM','hhttbbVBFl1cvv0cv1','hhttbbVBFl1cvv0p5cv1','hhttbbVBFl1cvv1p5cv1','hhttbbVBFl1cvv2cv1','hhttbbVBFl1cvv3cv1']
vars = ["mHH","SMVBFBDTScore","SMggFVBFBDTScore"]
colors =[(255, 0, 0), (208, 240, 193), (195, 138, 145), (155, 152, 204), (248, 206, 104),(120,0,204),(0, 255, 0),(255, 255, 0),(0, 0, 255),(255, 0, 255) ,(0, 255, 255),(128, 128, 123)]
colors = [ROOT.kRed+1, ROOT.kBlue+1, ROOT.kMagenta+1, ROOT.kOrange+1, ROOT.kYellow+1, ROOT.kCyan+1, ROOT.kGreen+1]
suffix = "_".join("2tag","0mHH","OS")
if not os.path.exists(outDir): os.makedirs(outDir)


#preselection

#Print basic informations
print(f"-->Working on the input file {path}")
file = uproot.open(path)
print(f"-->Will work on the following K2V samples: {K2Vs}")
print(f"-->Will work on the following variables: {vars}")

#setup style
aplt.set_atlas_style()

for cur_var in vars:
  print(f"---->Working on the variable {cur_var}")
  #initial the plotting
  fig, ax = aplt.subplots(1, 1)
  for cur_K2V, cur_color in zip(K2Vs, colors):
    print(f"------>Working on the K2V: {cur_K2V}")
    df_tmp = df
    df_tmp = df_tmp.Filter('(sample == \"{}\")'.format(cur_K2V))
    h = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{cur_var}_{cur_K2V}", f"{cur_var}", 200, -1, 1), f"{cur_var}", "weight"))
    Yields[cur_K2V] = df_tmp.Sum('weight').GetValue()
    histos[cur_K2V] = h.GetValue().Clone()
    histos[cur_K2V].Scale(1./Yields[cur_K2V])
    ax.plot(histos[cur_K2V], label=cur_K2V, linecolor = cur_color,  labelfmt="L")
  
  ax.add_margins(top=0.50)
  # Set axis titles
  ax.set_xlabel(cur_var)
  ax.set_ylabel("Normalised to unity")
  # Add the ATLAS Label
  aplt.atlas_label(text="Internal", loc="upper left")
  ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
  ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-}", size=22, align=13)
  # Add legend
  ax.legend(loc=(0.50, 0.65, 0.95, 0.92))
  fig.savefig(os.path.join(outDir,f"K2V_Compare_{cur_var}.pdf"))
  
exit(0)
# Create canvas with pads for main plot and data/MC ratio
c = ROOT.TCanvas("c", "", 800, 600)
pad = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
pad.SetTickx(False)
pad.SetTicky(False)
pad.Draw()
pad.cd() 
    
# Draw histos
for ch, color in zip(histos, [(255, 0, 0), (208, 240, 193), (195, 138, 145), (155, 152, 204), (248, 206, 104),(120,0,204),(0, 255, 0),(255, 255, 0),(0, 0, 255),(255, 0, 255) ,(0, 255, 255),(128, 128, 123)]):
  histos[ch].SetLineWidth(1)
  histos[ch].SetLineColor(ROOT.TColor.GetColor(*color))
  histos[ch].Draw("histo same")
histos['hhttbbVBFSM'].GetXaxis().SetTitle(f"{var}")
histos['hhttbbVBFSM'].GetYaxis().SetTitle("Normalised to 1")
histos['hhttbbVBFSM'].GetYaxis().SetLabelSize(0.04)
histos['hhttbbVBFSM'].GetYaxis().SetTitleSize(0.045)
histos['hhttbbVBFSM'].GetXaxis().SetLabelSize(0.04)
histos['hhttbbVBFSM'].GetXaxis().SetTitleSize(0.045)
histos['hhttbbVBFSM'].SetMinimum(0)
histos['hhttbbVBFSM'].SetMaximum(0.35)
histos['hhttbbVBFSM'].GetYaxis().ChangeLabel(1, -1, 0)

# Add legend
legend = ROOT.TLegend(0.60, 0.65, 0.92, 0.92)
legend.SetTextFont(42)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
legend.SetTextSize(0.02)
legend.SetTextAlign(32)
for ch in histos:
  legend.AddEntry(histos[ch], '{}\tYields: {:.2f}'.format(ch[6:],Yields[ch]) ,"f")
legend.Draw("SAME")

# Add ATLAS label
text = ROOT.TLatex()
text.SetNDC()
text.SetTextFont(72)
text.SetTextSize(0.045)
text.DrawLatex(0.21, 0.86, "ATLAS")
#text.DrawLatex(0.79, 0.14, "ATLAS Internal")
text.SetTextFont(42)
text.DrawLatex(0.21 + 0.16, 0.86, "Internal")
text.SetTextSize(0.04)
text.DrawLatex(0.21, 0.80, "#sqrt{s} = 13 TeV, Simulation")
text.DrawLatex(0.21, 0.74, "HH#rightarrowb#bar{b}#tau^{+}#tau^{-}")
c.SaveAs(f"K2V_Compare_{var}.png")
print("Saved figure to K2VCompare.png")



