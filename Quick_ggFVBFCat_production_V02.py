import os
import argparse
import sys
import ROOT
from decimal import Decimal
from tqdm import tqdm
import numpy as np

GGFVBFBDT='SMggFVBFBDTScore_V03'
#processes to save
Processes = ["hhttbb","hhttbbVBFSM"]
#Processes = ["Fake","data","ggFHtautau","hhttbb","DY","stopWt","stops","stopt","ttbar","ttbarFake","ttH","ttW","ttZ","hhttbbVBFl1cvv1cv1p5","hhttbbVBFl1cvv0p5cv1","hhttbbVBFl0cvv0cv1","hhttbbVBFl10cvv1cv1","hhttbbVBFl0cvv1cv1","hhttbbVBFl1cvv1cv0p5","hhttbbVBFl1cvv2cv1","hhttbbVBFl2cvv1cv1","hhttbbVBFl1cvv0cv1","hhttbbVBFl1cvv1p5cv1","hhttbbVBFSM","hhttbbVBFl1cvv3cv1","VBFHtautau","ggZHtautau","qqZHtautau","WHtautau","ggZHbb","qqZHbb","WHbb","qZHbb","W","WFake","WZ","WW","Wtt","WttFake","Zbc","Zbb","Zbl","Zcc","Zcl","Zl","ZZ","Zttbb","Zttbc","Zttbl","Zttl","Zttcl","Zttcc","DYtt"]
KRW_Processes = ['hhttbbKLRW0p0',"hhttbbKLRW20p0"]
#cuts = ["true", "mHH>350000.", "mHH<350000."]
regions = ["2tag_0_350mHH_OS_GGFSR", "2tag_350mHH_OS_GGFSR", "2tag_0mHH_OS_VBFSR"]
Hist = {}
#BDT_Names = ['SMVBFBDTScore','SM_NNscore','SMGGFBDTScore','SMGGFBDTScore_mHHsplit']
#BDT_Names = ['SMVBFBDTScore']
# Select events for the analysis
ROOT.gInterpreter.Declare("""
  int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
    int m_region = -1;
    if(!hasVBFJetCandidates){
      m_region = 0;
    }else{
      if(SMBDTggFVBF > cut ){
        m_region = 0;
      }else{
        m_region = 1;
      }
    }
    return m_region;
  }
  """)


########################
#Quick Projection:
########################
def binning2090():
  BDTbin990 = [0]*1990
  BDTbin101 = [0]*101
  BDTbinning = [0]*2091

  size3 = 1990
  size4 = 101

  for j in range(size3):
    BDTbin990[j] = np.round(j/1000.0 - 1.0,decimals=4)
    BDTbinning[j] = BDTbin990[j]

  l = j + 1

  for j in range(size4):
    BDTbin101[j] = np.round(0.99 + j/10000.0,decimals=4)
    BDTbinning[l] = BDTbin101[j]
    l += 1
  return np.double(BDTbinning)

def translator(in_name):
  out_name = in_name.replace("hhttbbKLRW","weight_klrw_")
  return out_name
def evaluate(score,df,proc,region,Weight):
  BDTbinning = binning2090()
  print(BDTbinning)
  #exit(0)

  if "VBFSR" in region:
    df_region = df.Filter(f"Category(hasVBFJetCandidates,{GGFVBFBDT},{score}) == 1")
    BDT="SMGGFBDTScore_mHHsplit_test2_extsh2211"
  elif "GGFSR" in region:
    df_region = df.Filter(f"Category(hasVBFJetCandidates,{GGFVBFBDT},{score}) == 0")
    BDT="SMGGFBDTScore_test2_extsh2211"
  else:
    print(f"---------->Region {region} is not supported")
    exit(2)

  #h = (df_region.Histo1D(ROOT.RDF.TH1DModel(f"{proc}", f"{proc}", 1000, -1, 1), f"{BDT}", Weight))
  h = (df_region.Histo1D(ROOT.RDF.TH1DModel(f"{proc}", f"{proc}", 2090, BDTbinning), f"{BDT}", Weight))
  Hist[proc] = h.GetValue().Clone()

def run(inFile,inFile_KRW,outDir,min_score,max_score,nbins,version,WP):
  score_range = (min_score,max_score)
  for cur_region in regions:
    if WP > -1 and WP < 1:
      print(f"---------->Evaluating WP = {np.round(WP,decimals=2)}")
      inner_run(cur_region,inFile,version,Processes,WP)
      inner_run(cur_region,inFile_KRW,version,KRW_Processes,WP)
      save(cur_region,outDir,WP)
    else:
      wid = (score_range[1]-score_range[0])/nbins
      #for i in tqdm(range(nbins)):
      for i in range(nbins):
        xi = np.round(score_range[0]+i*wid,decimals=2)
        #print(xi)
        inner_run(cur_region,inFile,version,Processes,xi)
        inner_run(cur_region,inFile_KRW,version,Processes,xi)
        save(cur_region,outDir,xi)
#def region_translator(region):
  
def inner_run(region,inFile,version,processes,WP):
  #Hist = {}
  print(f"-->Opening {inFile}")
  df = ROOT.RDataFrame("Nominal", inFile)
  print(f"---->Applying cut n_btag==2")
  df = df.Filter("n_btag==2")
  print(f"---->Applying cut OS==1")
  df = df.Filter("OS==1")
  print(f"---->Applying cut mBB < 150000")
  df = df.Filter('mBB < 150000.')
  for cur_proc in processes:
    print(f'------>Working on {cur_proc}')
    #df_tmp = df.Filter(f'(sample == \"{cur_proc}\")')
    if "KLRW" in cur_proc:
      Weight = translator(cur_proc)
      print(Weight)
      df_cut0 = df.Filter(f'(sample == "hhttbb")')
    else:
      Weight = "weight"
      df_cut0 = df.Filter(f'(sample == \"{cur_proc}\")')
    print(print(f'-------->Working on region {region}'))
    if ("2tag_0_350mHH" in region):
      df_cut1 = df_cut0.Filter('mHH < 350000.')
    elif("2tag_350mHH" in region):
      df_cut1 = df_cut0.Filter('mHH > 350000.')
    elif("2tag_0mHH" in region):
      df_cut1 = df_cut0.Filter('mHH > 0.')
    else:
      print(f"-------->Region {region} is not supported")
      exit(1)
   
    df_cut = df_cut1
    evaluate(np.round(WP,decimals=2),df_cut,cur_proc,region,Weight)

def save(region,outdir,WP):
  if not os.path.exists(outdir): os.makedirs(outdir)
  wp = str(np.round(WP,decimals=2)).replace("-","m").replace(".","p")
  out = os.path.join(outdir,"13TeV_TauLH_"+region+f"_SMBDT_{wp}.root")
  fout = ROOT.TFile.Open(out, "RECREATE")
  print(f"-->Saving {out}")
  for cur_hist in Hist:
    Hist[cur_hist].Write()

  fout.Close()

if __name__ == "__main__" :
  parser = argparse.ArgumentParser()
  parser.add_argument("--input", action = "store", dest = "inFile", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/ntuple_VBF03/tree-PCBT-ggfvbfV03-extsh2211.root')
  parser.add_argument("--input_KRW", action = "store", dest = "inFile_KRW", default = '/sps/atlas/a/angli/ATLAS/bbtautau/storage/ntuple_VBF03/tree-PCBT-ggfvbfV03-hhbbtt.root')
  parser.add_argument("--output", action = "store", dest = "outDir", default = "./outdir")
  parser.add_argument("--min_score", action = "store", dest = "min_score", default = "-1",type=float)
  parser.add_argument("--max_score", action = "store", dest = "max_score", default = "1",type=float)
  parser.add_argument("--number_bins", action = "store", dest = "nbins", default = "20",type=int)
  parser.add_argument("--version", action = "store", dest = "version", default = "scan")
  parser.add_argument("--WP", action = "store", dest = "WP", default = "999", type=float)
  args = vars(parser.parse_args())
  run(**args)


