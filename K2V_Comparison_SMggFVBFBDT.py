import os
import ROOT
import atlasplots as aplt
import uproot
#from helper import ofufbins, create_default_legend, calculate_separation_index

def print_bin_edges(histo):
  nbins = histo.GetNbinsX()
  print("Bin edges:")
  for i in range(1, nbins+1):
    low_edge = histo.GetXaxis().GetBinLowEdge(i)
    up_edge = histo.GetXaxis().GetBinUpEdge(i)
    print(f"[{low_edge}, {up_edge}]")

path = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/vbfbugfix/tree-ggfvbf.root"
outDir = '/sps/atlas/a/angli/ATLAS/bbtautau/plottingtool/vbfbugfix'
histos = {}
Yields = {}
histosVBFSR = {}
YieldsVBFSR = {}
#K2Vs=['hhttbbVBFSM','hhttbbVBFl1cvv0cv1','hhttbbVBFl1cvv0p5cv1','hhttbbVBFl1cvv1p5cv1','hhttbbVBFl1cvv2cv1','hhttbbVBFl1cvv3cv1','hhttbb']
#K2Vs=['hhttbbVBFSM','hhttbbVBFl0cvv0cv1', 'hhttbbVBFl1cvv0p5cv1', 'hhttbbVBFl10cvv1cv1', 'hhttbbVBFl2cvv1cv1', 'hhttbbVBFl1cvv3cv1', 'hhttbb']
#K2Vs=['hhttbbVBFSM',
#      'hhttbbVBFl0cvv0cv1',
#      'hhttbbVBFl0cvv1cv1',
#      'hhttbbVBFl10cvv1cv1',
#      'hhttbbVBFl1cvv0cv1',
#      'hhttbbVBFl1cvv0p5cv1',
#      'hhttbbVBFl1cvv1p5cv1',
#      'hhttbbVBFl1cvv2cv1',
#      'hhttbbVBFl1cvv3cv1',
#      'hhttbbVBFl2cvv1cv1',
#      'hhttbb'
#      ]
K2Vs=[#'hhttbb',
      'hhttbbVBFSM',
      #'hhttbbVBFl0cvv1cv1',
      #'hhttbbVBFl10cvv1cv1',
      #'hhttbbVBFl2cvv1cv1',
      'hhttbb'
      ]
vars = ["SMggFVBFBDTScore_V03"]
#vars=["MHH"]
#vars = ["mHH","SMVBFBDTScore_V03","SMggFVBFBDTScore",
#        "mBB","mMMC","mHH","mHHStar","mHH_scaled","dRTauLep","dPtLepTau","dRBB","dPhiLepTauBB","LeadBjet","SubLeadBjet","mTW","MET","METCent","dPhiLepMET","dPhiTauTauMET","HT","ST","MT2","pTBB","pTTauLep","pTTauTau","pTHH","minDRbl","minDRbtau","METSig","METSigPU","TauPt","TauEta","TauPhi","LepPt","LepEta","LepPhi","pTB0Uncorr","pTB1Uncorr","LeadJetPt","Mb0lep","Mb1lep","Mb0tau","Mb1tau","EtaBB","EtaTauTau","dPhiTauMET","dEtalepTau","LeadBjetEta","LeadBjetPhi","SubLeadBjetEta","SubLeadBjetPhi","METPhi","coshelicitybb","coshelicitytautau","cosTheta","dRb0tau","dRb1tau","dRb0lep","dRb1lep","dPhibbMET","dEtaBB","dEtaHH","dRHH","mTtau","mTLep","T1","T2",
#        "mjjVBF","dRjjVBF","pTj1VBF","pTj2VBF","pTjjVBF","Etaj1VBF","Etaj2VBF","dEtajjVBF","dPhijjVBF","pTSixObjVBF","multEtajjVBF",
#        "mEff_bbtt","cent_bbtt","apla_bbtt","spher_bbtt","fwm0_bbtt","fwm1_bbtt","fwm2_bbtt","fwm3_bbtt","fwm4_bbtt","hcm1_bbtt","hcm2_bbtt","hcm3_bbtt","hcm4_bbtt","hcm5_bbtt","hcm6_bbtt","hcm7_bbtt","hcm8_bbtt","plan_bbtt","varc_bbtt","vard_bbtt","circ_bbtt","pflow_bbtt","thrust_bbtt",
#        "mEff_ttj","cent_ttj","apla_ttj","spher_ttj","fwm0_ttj","fwm1_ttj","fwm2_ttj","fwm3_ttj","fwm4_ttj","hcm1_ttj","hcm2_ttj","hcm3_ttj","hcm4_ttj","hcm5_ttj","hcm6_ttj","hcm7_ttj","hcm8_ttj","plan_ttj","varc_ttj","vard_ttj","circ_ttj","pflow_ttj","thrust_ttj",
#        "mEff_ttjf","cent_ttjf","apla_ttjf","spher_ttjf","fwm0_ttjf","fwm1_ttjf","fwm2_ttjf","fwm3_ttjf","fwm4_ttjf","hcm1_ttjf","hcm2_ttjf","hcm3_ttjf","hcm4_ttjf","hcm5_ttjf","hcm6_ttjf","hcm7_ttjf","hcm8_ttjf","plan_ttjf","varc_ttjf","vard_ttjf","circ_ttjf","pflow_ttjf","thrust_ttjf"
#        #"mHH", "mjjVBF", "fwm0_ttjf", "mEff_ttjf", "dEtaHH", "dPhiLepTauBB", "fwm0_ttj", "pTj1VBF", "hcm4_ttj", "pTHH", "mHHStar", "fwm0_bbtt", "MT2", "dEtaBB", "hcm2_ttjf", "hcm3_ttjf", "hcm1_ttjf", "dEtajjVBF", "multEtajjVBF", "pflow_bbtt",
#]
colors = [ROOT.kRed+1, ROOT.kBlue+1, ROOT.kMagenta+1, ROOT.kOrange+1, ROOT.kGreen+1, ROOT.kYellow+1, ROOT.kCyan+1, ROOT.kMagenta-7,ROOT.kRed-6,ROOT.kBlue-7,ROOT.kGreen+4,ROOT.kGreen+1]
if not os.path.exists(outDir): os.makedirs(outDir)
#preselection
df = ROOT.RDataFrame("Nominal", path)
df = df.Filter('mBB < 150000.')
print(df.Count().GetValue())
df = df.Filter("n_btag==2")
print(df.Count().GetValue())
df = df.Filter('OS==1')
print(df.Count().GetValue())
df = df.Filter('!sample.empty()')
print(df.Count().GetValue())
df = df.Filter('mHH>0.')
df = df.Define("MHH", "mHH/1000.")
print(df.Count().GetValue())
df = df.Filter("hasVBFJetCandidates==1")
print(df.Count().GetValue())
#Print basic informations
print(f"-->Working on the input file {path}")
print(f"-->Will work on the following K2V samples: {K2Vs}")
print(f"-->Will work on the following variables: {vars}")

#setup style
ROOT.gROOT.SetBatch()
aplt.set_atlas_style()

for cur_var in vars:
  print(f"---->Working on the variable {cur_var}")
  #initial the plotting
  fig, ax = aplt.subplots(1, 1)
  for cur_K2V, cur_color in zip(K2Vs, colors):
    print(f"------>Working on the K2V: {cur_K2V}")
    df_tmp = df
    df_tmp = df_tmp.Filter('(sample == \"{}\")'.format(cur_K2V))
    #h = (df_tmp.Histo1D(f"{cur_var}", "weight"))
    h = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{cur_var}_{cur_K2V}", f"{cur_var}", 20, -1, 1), f"{cur_var}", "weight"))
    #h = (df_tmp.Histo1D(ROOT.RDF.TH1DModel(f"{cur_var}_{cur_K2V}", f"{cur_var}", 20, -1, 1), f"{cur_var}"))
    Yields[cur_K2V] = df_tmp.Sum('weight').GetValue()
    print(Yields[cur_K2V])
    #Yields[cur_K2V] = df_tmp.Count().GetValue()
    histos[cur_K2V] = h.GetValue().Clone()
    #print_bin_edges(histos[cur_K2V])
    #exit(0)
    #histos[cur_K2V] = h.Clone()
    histos[cur_K2V].Scale(1./Yields[cur_K2V])
    print(histos[cur_K2V].Integral())
    ax.plot(histos[cur_K2V], label=cur_K2V, linecolor = cur_color,  labelfmt="L")
  
  ax.add_margins(top=0.30)
  # Set axis titles
  ax.set_xlabel(f"{cur_var}")
  ax.set_ylabel("Normalised to unity")
  # Add the ATLAS Label
  aplt.atlas_label(text="Internal", loc="upper left")
  ax.text(0.2, 0.86, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=22, align=13)
  ax.text(0.2, 0.81, "HH#rightarrowb#bar{b}#tau_{lep}#tau_{had}", size=22, align=13)
  # Add legend
  #ax.legend(loc=(0.50, 0.50, 0.95, 0.92))
  ax.legend(loc=(0.45, 0.55, 1 - ROOT.gPad.GetRightMargin() - 0.05, 1 - ROOT.gPad.GetTopMargin() - 0.05))
  fig.savefig(os.path.join(outDir,f"K2V_Compare_{cur_var}.pdf"))
  


