import os
from datetime import datetime
import numpy as np
import ROOT
import pandas as pd
import matplotlib.pyplot as plt

def calculate_significance(signal, background):
    if background > 0:
        return signal / np.sqrt(signal + background)
    else:
        return 0

def calculate_data(rdf, WP_values, signal_regions, processes):
    data = []
    for WP in WP_values:
        print(f"-->Working on WP: {WP}")
        for region_name, region_selection in signal_regions.items():
            print(f"---->Working on region: {region_name}")
            region_selection_formatted = region_selection.format(BDT="SMggFVBFBDTScore_V03", WP=str(WP))

            rdf_region = rdf.Filter(region_selection_formatted)

            signal = 0
            background = 0

            for process, samples in processes.items():
                print(f"------>Working on process: {process}")
                sample_selection = " || ".join([f'sample=="{sample}"' for sample in samples])
                rdf_sample = rdf_region.Filter(sample_selection)
                weighted_yield = rdf_sample.Sum("weight").GetValue()

                if (region_name in ["low-mHH-GGFSR", "high-mHH-GGFSR"] and process == "ggF") or (region_name == "VBFSR" and process == "VBFSM"):
                    signal += weighted_yield
                elif process not in ["ggF", "ggFKL10", "VBFSM"]:
                    background += weighted_yield

            significance = calculate_significance(signal, background)
            if signal + background > 0:
                efficiency = signal / (signal + background)
            else:
                efficiency = 0
            data.append({"WP": WP, "Region": region_name, "Significance": significance, "Efficiency": efficiency})
    return data



def create_plots(data, WP_values, signal_regions, outDir):
    print("-->Creating plots")
    plt.figure(figsize=(10, 5))

    # Significance plot
    plt.subplot(121)
    for region_name in signal_regions:
        plt.plot(WP_values, data[data["Region"] == region_name]["Significance"], label=region_name)
    plt.xlabel("WP")
    plt.ylabel("Significance")
    plt.legend()
    plt.title("Significance vs WP")

    # Efficiency plot
    plt.subplot(122)
    for region_name in signal_regions:
        plt.plot(WP_values, data[data["Region"] == region_name]["Efficiency"], label=region_name)
    plt.xlabel("WP")
    plt.ylabel("Efficiency")
    plt.legend()
    plt.title("Efficiency vs WP")

    plt.tight_layout()

    output_file_path = os.path.join(outDir, "Significance_Efficiency_Plots.png")
    plt.savefig(output_file_path)

def main():
    InPut = "/sps/atlas/a/angli/ATLAS/bbtautau/storage/Reader_EBR_V02/tree-extsh2211.root"
    rdf = ROOT.RDataFrame("Nominal", InPut)

    # Add preselection
    PreSelection = "OS==1&&n_btag==2&&mBB<150000"
    rdf = rdf.Filter(PreSelection)

    # Define your processes, signal regions, and category here
    processes = {
      "ggF": ['hhttbb'],
      "ggFKL10": ['hhttbbL10'],
      "VBFSM": ['hhttbbVBFSM'],
      "Diboson": ["WZ", "ZZ", "WW"],
      "Zttjets": ["Zttbb", "Zttbl", "Zttbc", "Zttl", "Zttcc", "Zttcl"],
      "Zjets": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc"],
      "Stop": ["stops", "stopt", "stopWt"],
      "ttV": ["ttW", "ttZ"],
      #"DY": ["DY", "DYtt"],
      "SHiggs": ["ttH", "ggFHtautau", "VBFHtautau", "ggZHtautau", "qqZHbb", "WHbb", "WHtautau", "ggZHbb", "qqZHtautau"],
      "ttbar": ["ttbar"],
      "Others": ["Zcl", "Zcc", "Zbl", "Zbb", "Zbc", "Zl", "W", "W", "Wtt","DY", "DYtt"],
      "Fakes": ["WFake", "WttFake", "ttbarFake"],
    }

    signal_regions = {
      "low-mHH-GGFSR":    f"mHH<350000&&Category(hasVBFJetCandidates,{{BDT}},{{WP}})==0",
      "high-mHH-GGFSR":   f"mHH>350000&&Category(hasVBFJetCandidates,{{BDT}},{{WP}})==0",
      "VBFSR": f"Category(hasVBFJetCandidates,{{BDT}},{{WP}})==1",
    }
 
    ROOT.gInterpreter.Declare("""
    int Category(bool hasVBFJetCandidates, float SMBDTggFVBF, float cut){
      int m_region = -1;
      if(!hasVBFJetCandidates){
        m_region = 0;
      }else{
        if(SMBDTggFVBF > cut ){
          m_region = 0;
        }else{
          m_region = 1;
        }
      }
      return m_region;
    }
    """)
    # Add an option to choose the number of WP points for the calculation
    num_WP_points = 21  # Set the desired number of WP points
    WP_values = np.linspace(-1, 1, num_WP_points)

    print("-->Calculating data")
    data = calculate_data(rdf, WP_values, signal_regions, processes)
    df = pd.DataFrame(data)

    outDir = f"./Significance_EBR_V02_tree_{datetime.now().strftime('%Y%m%d')}"
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    df.to_csv(os.path.join(outDir, "Significance_Efficiency_Data.csv"), index=False)

    print("-->Creating plots")
    create_plots(df, WP_values, signal_regions, outDir)
    print("Done!")

if __name__ == "__main__":
    main()

